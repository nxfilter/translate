<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(PolicyCxblockDao dao){
	PolicyCxblockData data = new PolicyCxblockData();
	data.enableFilter = paramBoolean("enableFilter");
	data.blockIpHost = paramBoolean("blockIpHost");
	data.blockedKeyword = paramString("blockedKeyword");
	data.cacheTtl = paramInt("cacheTtl");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
PolicyCxblockDao dao = new PolicyCxblockDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
PolicyCxblockData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<div class="title">Chromebook</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Enable Filter</td>
		<td>
		<input type="checkbox" class="no-border" name="enableFilter"
			<%if(data.enableFilter){out.print("checked");}%>> Chromebook filtering requires CxBlock running on Chromebooks.
			</td>
	</tr>

	<tr>
		<td>Block IP host</td>
		<td><input type="checkbox" class="no-border" name="blockIpHost"
			<%if(data.blockIpHost){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Blocked Keyword</td>
		<td>
			This is for URL level filtering by keywords. You can add multiple keywords separated by spaces.<br>
			<textarea name="blockedKeyword" cols="80" rows="8"><%= data.blockedKeyword%></textarea>
		</td>
	</tr>

	<tr>
		<td>Query Cache TTL</td>
		<td><input type="text" name="cacheTtl" value="<%= data.cacheTtl%>" size="2"> seconds, 60 ~ 3600</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
