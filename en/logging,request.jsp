<%@include file="include/top.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();
permission.addReport();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
RequestDao dao = new RequestDao();

// Set filtering option.
dao.limit = 100;
dao.page = paramInt("page", 1);
dao.stime = paramString("stime");
dao.etime = paramString("etime");

dao.domain = paramString("domain");
dao.user = paramString("user");
dao.grp = paramString("grp");

dao.cltIp = paramString("cltIp");
dao.policy = paramString("policy");
dao.category = paramString("category");
dao.blockFlag = paramBoolean("blockFlag");

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("csv")){
	String filename = "logging-request.csv";

	// Don't make it too big.
	dao.limit = 100000;
	if(dao.writeCsvFile(filename)){
		response.sendRedirect("download.jsp?filename=" + filename + "&contentType=text/csv");
		return;
	}
	else{
		errList.add("Couldn't write the file.");
	}
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gTimeOption = paramString("timeOption", "2h");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionCsv(){
	var form = document.goForm;
	form.actionFlag.value = "csv";
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function setUserdef(form){
	form.timeOption[0].checked = true;
}

//-----------------------------------------------
function setPeriod(form){
	var opt = radioGetValue(form.timeOption);
	if(opt == "userdef"){
		return;
	}

	var stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 2)%>';
	var etime = '<%= strftime("yyyy/MM/dd HH:mm")%>';

	if(opt == "24h"){
		stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 24)%>';
	}

	if(opt == "48h"){
		stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 48)%>';
	}

	form.stime.value = stime;
	form.etime.value = etime;
	form.stime.disabled = false;
	form.etime.disabled = false;
}

//-----------------------------------------------
function setPeriod2(form){
	if(!radioIsChecked(form.timeOption) || form.timeOption[0].checked){
		return;
	}
	setPeriod(form);
}

//-----------------------------------------------
function clearSearchForm(){
	document.searchForm.domain.value = "";
	document.searchForm.user.value = "";
	document.searchForm.cltIp.value = "";
	document.searchForm.grp.value = "";

	document.searchForm.policy.value = "";
	document.searchForm.category.value = "";
	document.searchForm.blockFlag.checked = false;
}

//-----------------------------------------------
function searchByUser(user){
	clearSearchForm();
	document.searchForm.user.value = user;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByCltIp(cltIp){
	clearSearchForm();
	document.searchForm.cltIp.value = cltIp;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByGrp(grp){
	clearSearchForm();
	document.searchForm.grp.value = grp;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByPolicy(policy){
	clearSearchForm();
	document.searchForm.policy.value = policy;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByCategory(category){
	clearSearchForm();
	document.searchForm.category.value = category;
	document.searchForm.submit();
}

//-----------------------------------------------
function setPopupForm(domain){
	document.popupForm.domain.value = domain;
	document.popupForm.id.value = 0;
	document.popupForm.chkSubdomain.checked = false;
	$("#popupResult").empty();
}
</script>

<!-- view -->
<form name="searchForm" action="<%= getPageName()%>" method="get">
<input type="hidden" name="actionFlag" value="">
<div class="title">Request</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="100" align="right">Time :</td>
		<td colspan="7">
			<input id="stime" type="text" name="stime" value="<%= dao.getStime()%>" size="12" onchange="javascript:setUserdef(this.form)">
				~ <input id="etime" type="text" name="etime" value="<%= dao.getEtime()%>" size="12" onchange="javascript:setUserdef(this.form)">
			
			<div style="display: none;">
				<input type="radio" class="no-border" name="timeOption" value="userdef" onclick="javascript:setPeriod(this.form)"
					<%if(gTimeOption.equals("userdef")){out.print("checked");}%>> User defined
			</div>

			<input type="radio" class="no-border" name="timeOption" value="2h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("2h")){out.print("checked");}%>> Last 2 hours
			<input type="radio" class="no-border" name="timeOption" value="24h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("24h")){out.print("checked");}%>> Last 24 hours
			<input type="radio" class="no-border" name="timeOption" value="48h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("48h")){out.print("checked");}%>> Last 48 hours
		</td>
	</tr>

	<tr>
		<td width="100" align="right">Domain :</td>
		<td width="100"><input type="text" name="domain" value="<%= dao.domain%>" size="25"></td>
		
		<td width="100" align="right">User :</td>
		<td width="100"><input type="text" name="user" value="<%= dao.user%>" size="25"></td>

		<td width="100" align="right">Client IP :</td>
		<td width="100"><input type="text" name="cltIp" value="<%= dao.cltIp%>" size="25"></td>
		
		<td width="60"></td>
		<td ></td>
	</tr>

	<tr>
		<td align="right">Group :</td>
		<td><input type="text" name="grp" value="<%= dao.grp%>" size="25"></td>

		<td align="right">Policy :</td>
		<td><input type="text" name="policy" value="<%= dao.policy%>" size="25"></td>

		<td align="right">Category :</td>
		<td><input type="text" name="category" value="<%= dao.category%>" size="25"></td>
		
		<td align="right">Block :</td>
		<td><input type="checkbox" class="no-border" name="blockFlag" <%if(dao.blockFlag){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td></td>
		<td></td>
		<td colspan="5">
<input type="button" value="SUBMIT" onclick="javascript:setPeriod2(this.form);this.form.actionFlag.value='';this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="CLEAR" onclick="javascript:clearSearchForm();">
<input type="button" value="CSV-EXPORT" onclick="javascript:actionCsv();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%> / Page : <%= dao.page%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="11"></td></tr>
	<tr class="head">
		<td width="90">Time</td>
		<td width="50">Block</td>
		<td width="50" align="center">Count</td>
		<td width="60" align="center">Type</td>
		<td width="350">Domain</td>
		<td width="200">User</td>
		<td width="150">Client IP</td>
		<td width="150">Group</td>
		<td width="150">Policy</td>
		<td width="200">Category</td>
		<td width="">Reason</td>
	</tr>
	<tr class="line"><td colspan="11"></td></tr>
<%
List<RequestData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='11' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	RequestData data = dataList.get(i);

	String categoryLine = data.category;
	if(categoryLine.length() > 30){
		categoryLine = safeSubstring(categoryLine, 30) + "..";
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='11'></td></tr>");
	}

	String domainStyle = "";
	if(data.getBlockYn().equals("Y")){
		domainStyle = "background-color:#AFE0EC;padding:2px;color:#000000";
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td align="center"><%= data.getBlockYn()%></td>
		<td align="right"><%= data.cnt%>&nbsp;&nbsp;</td>
		<td align="center"><%= data.getTypeCode()%></td>
		<td><a href="#recat-popup" class="open-popup-link" title="Click for reclassification"
			onclick="javascript:setPopupForm('<%= data.domain%>')" style="<%= domainStyle%>"><%= data.domain%></a></td>
		<td><a href="javascript:searchByUser('<%= data.user%>')"><%= data.user%></a></td>
		<td><a href="javascript:searchByCltIp('<%= data.cltIp%>')"><%= data.cltIp%></a></td>
		<td><a href="javascript:searchByGrp('<%= data.grp%>')"><%= data.grp%></a></td>
		<td><a href="javascript:searchByPolicy('<%= data.policy%>')"><%= data.policy%></a></td>
		<td title='<%= data.category%>'><%= categoryLine%></td>
		<td><%= data.getReason()%></td>
	</tr>
<%}%>

	<tr class="line"><td colspan="11"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="timeOption" value="<%= gTimeOption%>">
<input type="hidden" name="stime" value="<%= dao.stime%>">
<input type="hidden" name="etime" value="<%= dao.etime%>">
<input type="hidden" name="domain" value="<%= dao.domain%>">
<input type="hidden" name="user" value="<%= dao.user%>">
<input type="hidden" name="grp" value="<%= dao.grp%>">
<input type="hidden" name="cltIp" value="<%= dao.cltIp%>">
<input type="hidden" name="policy" value="<%= dao.policy%>">
<input type="hidden" name="category" value="<%= dao.category%>">
<input type="hidden" name="blockFlag" value="<%= dao.blockFlag%>">
</form>
<!-- /goForm -->

<!-- popupForm -->
<div id="recat-popup" class="xrecat-popup mfp-hide">
	Add domain into a new category.
	</p>

	<form id="popupForm" name="popupForm" method="post" action="category,system_edit.jsp">
	<input type="hidden" name="actionFlag" value="addDomain">
	<table width="100%" cellpadding="2">

		<tr>
			<td>
			<input id="popuFormDomain" type="text" name="domain" size="50"/><br>
			</td>
		</tr>

		<tr>
			<td>
			<select name="id">
			<option value="0"> Select a category to move in
<%
List<CategoryData> catList = new CategorySystemDao().selectList();

// Add custom categories.
catList.addAll(new CategoryCustomDao().selectList());

for(CategoryData data : catList){
	printf("<option value='%s'> %s", data.id, data.name);
}
%>
			</select>
			</td>
		</tr>

		<tr>
			<td>
			Include subdomains <input id="popupFormChkSubdomain" name="chkSubdomain" type="checkbox"/><br>
			</td>
		</tr>

		<tr>
			<td>
				<input type="submit" value="SUBMIT">
				<input type="button" value="CLOSE" onclick="javascript:$.magnificPopup.close();">
			</td>
		</tr>

	</table>

	</p>
	<div id="popupResult" class="succ-msg"></div>
</div>
<!-- /popupForm -->

<%@include file="include/bottom.jsp"%>

<!-- Datetime picker -->
<script type="text/javascript">
//-----------------------------------------------
var dateToDisable = new Date();

//-----------------------------------------------
jQuery("#stime").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});

//-----------------------------------------------
jQuery("#etime").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});

//-----------------------------------------------
$(".open-popup-link").magnificPopup({
	type:"inline",
	midClick: true
});

//-----------------------------------------------
$("#popupForm").submit(function(event){
	event.preventDefault();
 
	// Get form values.
	var form = $(this);
	var url = form.attr("action");

	var data = {};
	form.find("[name]").each(function(i , v){
		var input = $(this),
		name = input.attr("name"),
		value = input.val();
		data[name] = value;
	});

	if(data["id"] == 0){
		return;
	}

	$.post(url, data);
	$("#popupResult").empty().append("Submitted.");
});

//-----------------------------------------------
var prevDomain = "";
$("#popupFormChkSubdomain").change(function(){
	if($(this).is(":checked")){
		var domain = $("#popuFormDomain").val();
		if(domain.indexOf("*.") != 0){
			prevDomain = domain;
		}

		domain = domain.replace(/^www\./, "");
		domain = "*." + domain;

		$("#popuFormDomain").val(domain);
		return;
	}
	$("#popuFormDomain").val(prevDomain);
});
</script>
