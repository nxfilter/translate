<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
boolean checkParam(ConfigData data){
	// Redirection IP.
	if (!ParamTest.isValidBlockIp(data.blockRediIp)) {
	    errList.add("Invalid block redirection IP.");
	    return false;
	}

	if (isNotEmpty(data.rfBlockRediIp)) {
	    if (!ParamTest.isValidBlockIp(data.rfBlockRediIp)) {
		    errList.add("Invalid external redirection IP.");
			return false;
	    }
	}

	if (isNotEmpty(data.ipv6BlockRediIp)) {
	    if (!isValidIpv6(data.ipv6BlockRediIp)) {
		    errList.add("Invalid IPv6 redirection IP.");
			return false;
	    }
	}

	// Login, logout domain.
	if (!isValidDomain(data.loginDomain)) {
		errList.add("Invalid login domain.");
		return false;
	}

	if (!isValidDomain(data.logoutDomain)) {
		errList.add("Invalid logout domain.");
		return false;
	}

	// Syslog.
	if (isNotEmpty(data.syslogHost) && !isValidIp(data.syslogHost)) {
		errList.add("Invalid IP address for Syslog host.");
		return false;
	}

	if (data.remoteLogging && isEmpty(data.syslogHost)) {
	    errList.add("Remote logging option requires Syslog host.");
	    return false;
	}

	// Netflow.
	if (isNotEmpty(data.netflowIp) && !isValidIp(data.netflowIp)) {
		errList.add("Invalid router IP.");
		return false;
	}

	if (data.useNetflow && isEmpty(data.netflowIp)) {
	    errList.add("Router IP missing.");
	    return false;
	}

	// Misc.
	if (!isValidDomain(data.adminDomain)) {
	    errList.add("Invalid admin domain.");
	    return false;
	}

	return true;
}

//-----------------------------------------------
void update(ConfigDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	ConfigData data = new ConfigData();

	// Block and authentication.
    data.blockRediIp = paramString("blockRediIp");
    data.rfBlockRediIp = paramString("rfBlockRediIp");
    data.ipv6BlockRediIp = paramString("ipv6BlockRediIp");
    data.enableLogin = paramBoolean("enableLogin");
    data.disableLoginRedirection = paramBoolean("disableLoginRedirection");
    data.loginDomain = paramString("loginDomain");
    data.logoutDomain = paramString("logoutDomain");
    data.loginSessionTtl = paramInt("loginSessionTtl");

	// Syslog.
    data.syslogHost = paramString("syslogHost");
    data.syslogPort = paramInt("syslogPort");
    data.exportBlockedOnly = paramBoolean("exportBlockedOnly");
    data.fromEachNode = paramBoolean("fromEachNode");
    data.remoteLogging = paramBoolean("remoteLogging");

    // Netflow.
	data.netflowIp = paramString("netflowIp");
    data.netflowPort = paramInt("netflowPort");
    data.useNetflow = paramBoolean("useNetflow");

	// Misc.
    data.adminDomain = paramString("adminDomain");
    data.bypassMsUpdate = paramBoolean("bypassMsUpdate");
    data.logRetentionDays = paramInt("logRetentionDays");
    data.sslOnly = paramBoolean("sslOnly");
	data.autoBackupDays = paramInt("autoBackupDays");
	data.agentPolicyUpdatePeriod = paramInt("agentPolicyUpdatePeriod");

	// Validate and update it.
	if(checkParam(data) && dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ConfigDao dao = new ConfigDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
ConfigData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">Block and Authentication</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Block Redirection IP</td>
		<td><input type="text" name="blockRediIp" value="<%= data.blockRediIp%>" size="25"></td>
	</tr>

	<tr>
		<td>External Redirection IP</td>
		<td><input type="text" name="rfBlockRediIp" value="<%= data.rfBlockRediIp%>" size="25"></td>
	</tr>

	<tr>
		<td>IPv6 Redirection IP</td>
		<td><input type="text" name="ipv6BlockRediIp" value="<%= data.ipv6BlockRediIp%>" size="25"></td>
	</tr>

	<tr>
		<td>Enable Authentication</td>
		<td><input type="checkbox" class="no-border"
			name="enableLogin"	<%if(data.enableLogin){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Login Domain</td>
		<td><input type="text" name="loginDomain" value="<%= data.loginDomain%>" size="25"></td>
	</tr>

	<tr>
		<td>Logout Domain</td>
		<td><input type="text" name="logoutDomain" value="<%= data.logoutDomain%>" size="25"></td>
	</tr>

	<tr>
		<td>Login Session TTL</td>
		<td><input type="text" name="loginSessionTtl" size="4" maxlength="4"
			value="<%= data.loginSessionTtl%>"> minutes, 5 ~ 1440</td>
	</tr>

	<tr>
		<td>Disable Login Redirection</td>
		<td><input type="checkbox" class="no-border"
			name="disableLoginRedirection"	<%if(data.disableLoginRedirection){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Syslog</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Syslog Host</td>
		<td>
		You need to restart NxFilter after changing Syslog setup.<br>
		<input type="text" name="syslogHost" value="<%= data.syslogHost%>" size="25"></td>
	</tr>

	<tr>
		<td>Syslog Port</td>
		<td><input type="text" name="syslogPort" size="4" maxlength="4"
			value="<%= data.syslogPort%>"></td>
	</tr>

	<tr>
		<td>Export Blocked Only</td>
		<td><input type="checkbox" class="no-border"
			name="exportBlockedOnly" <%if(data.exportBlockedOnly){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>From Each Node</td>
		<td><input type="checkbox" class="no-border"
			name="fromEachNode" <%if(data.fromEachNode){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Enable Remote Logging</td>
		<td><input type="checkbox" class="no-border"
			name="remoteLogging" <%if(data.remoteLogging){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">NetFlow</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Router IP</td>
		<td>
		You need to restart NxFilter after changing NetFlow setup.<br>
		<input type="text" name="netflowIp" value="<%= data.netflowIp%>" size="25"></td>
	</tr>

	<tr>
		<td>Listen Port</td>
		<td><input type="text" name="netflowPort" value="<%= data.netflowPort%>" size="2"></td>
	</tr>

	<tr>
		<td>Run Collector</td>
		<td><input type="checkbox" class="no-border"
			name="useNetflow" <%if(data.useNetflow){out.print("checked");}%>>
			</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Misc</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Admin Domain</td>
		<td><input type="text" name="adminDomain" value="<%= data.adminDomain%>" size="25"></td>
	</tr>

	<tr>
		<td>Bypass Microsoft Update</td>
		<td><input type="checkbox" class="no-border"
			name="bypassMsUpdate" <%if(data.bypassMsUpdate){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Log Retention Days</td>
		<td><input type="text" name="logRetentionDays" size="2" maxlength="3"
			value="<%= data.logRetentionDays%>"> days, 0 ~ 400</td>
	</tr>

	<tr>
		<td>SSL Only To Admin GUI</td>
		<td><input type="checkbox" class="no-border"
			name="sslOnly" <%if(data.sslOnly){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Auto Backup</td>
		<td><input type="text" name="autoBackupDays" size="2" maxlength="2"
			value="<%= data.autoBackupDays%>"> days, 0 ~ 30</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<%@include file="include/bottom.jsp"%>
