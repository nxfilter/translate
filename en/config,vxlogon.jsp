<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
boolean checkParam(VxlogonData data){
	// logon, logoff domain.
	if (!isValidDomain(data.logonDomain)) {
		errList.add("Invalid logon domain.");
		return false;
	}

	if (!isValidDomain(data.logoffDomain)) {
		errList.add("Invalid logoff domain.");
		return false;
	}

	return true;
}

//-----------------------------------------------
void update(VxlogonDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	VxlogonData data = new VxlogonData();

    data.logonDomain = paramString("logonDomain");
    data.logoffDomain = paramString("logoffDomain");
	data.useVxlogon = paramBoolean("useVxlogon");

	// Validate and update it.
	if(checkParam(data) && dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
VxlogonDao dao = new VxlogonDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
VxlogonData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">VxLogon</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height: 20px">
VxLogon is a script version of Active Directory single sign-on agent.
</div>
		</td>
	</tr>

	<tr>
		<td width="210">Logon Domain</td>
		<td><input type="text" name="logonDomain" value="<%= data.logonDomain%>" size="25"></td>
	</tr>

	<tr>
		<td>Logoff Domain</td>
		<td><input type="text" name="logoffDomain" value="<%= data.logoffDomain%>" size="25"></td>
	</tr>

	<tr>
		<td>Use VxLogon</td>
		<td><input type="checkbox" class="no-border"
			name="useVxlogon"	<%if(data.useVxlogon){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<%@include file="include/bottom.jsp"%>
