<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(BlockPageDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	BlockPageData data = new BlockPageData();

	// We use requestString here to preserve all the special characters.
	data.blockPage = requestString("blockPage");
	data.loginPage = requestString("loginPage");
	data.welcomePage = requestString("welcomePage");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
BlockPageDao dao = new BlockPageDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("restore")){
	if(dao.restoreDefault()){
		succList.add("Data updated.");
	}
}

// Global.
BlockPageData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function preview(text){
	var w = window.open("", "previewWindow", "width=1024,height=600");
	w.document.open();
	w.document.write(text);
	w.document.close();
}

//-----------------------------------------------
function restoreDefault(form){
	form.actionFlag.value = "restore";
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<div class="title">Block Page</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height:20px;">
You can edit your block-page, login-page, welcome-page here.<br>
When you edit these pages do not touch the template variables between '\#{' and '}'.<br>
&nbsp;&nbsp;ex) \#{domain}, \#{reason}
</div>
		</td>
	</tr>

	<tr>
		<td colspan="2">
&nbsp;
		</td>
	</tr>

	<tr>
		<td width="200">Block Page</td>
		<td>
			<textarea name="blockPage" cols="80" rows="8"><%= escapeHtml(data.blockPage)%></textarea>
			<input type="button" value="PREVIEW" onclick="javascript:preview(this.form.blockPage.value);">
		</td>
	</tr>

	<tr>
		<td colspan="2">
		</td>
	</tr>

	<tr>
		<td>Login Page</td>
		<td>
			<textarea name="loginPage" cols="80" rows="8"><%= escapeHtml(data.loginPage)%></textarea>
			<input type="button" value="PREVIEW" onclick="javascript:preview(this.form.loginPage.value);">
		</td>
	</tr>

	<tr>
		<td colspan="2">
		</td>
	</tr>

	<tr>
		<td>Welcome Page</td>
		<td>
			<textarea name="welcomePage" cols="80" rows="8"><%= escapeHtml(data.welcomePage)%></textarea>
			<input type="button" value="PREVIEW" onclick="javascript:preview(this.form.welcomePage.value);">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="RESTORE-DEFAULT" onclick="javascript:restoreDefault(this.form);">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
