<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(CategoryCustomDao dao){
	CategoryData data = new CategoryData();
	data.name = paramString("name");
	data.description = paramString("description");

	// Param validation.
	if(!ParamTest.isValidNameLen(data.name)){
		errList.add(ParamTest.ERR_NAME_LEN);
		return;
	}
	
	if(!ParamTest.isValidUsernameChar(data.name)){
		errList.add(ParamTest.ERR_NAME_CHAR);
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(CategoryCustomDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
CategoryCustomDao dao = new CategoryCustomDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

if(actionFlag.equals("export")){
	String filename = "catcustom_" + strftime("yyyyMMddHHmm") + ".txt";

	if(new CategorySystemDao().exportFile(filename, false)){
		response.sendRedirect("download.jsp?filename=" + filename + "&contentType=text/plain");
		return;
	}
	else{
		errList.add("Couldn't write the file.");
	}
}

// If it's about importation.
int importCount = paramInt("importCount");
if(importCount > 0){
	if(actionFlag.equals("ruleset")){
		succList.add(importCount + " classification rules imported.");
	}
	else{
		succList.add(importCount + " domains imported.");
	}
}

// Global.
int gCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionExport(){
	var form = document.goForm;
	form.actionFlag.value = "export";
	form.submit();
}

//-----------------------------------------------
function actionImport(uploadForm){
	if(uploadForm.file1.value == ""){
		alert("No file selected.");
		return;
	}

	uploadForm.action = "import.jsp";
	uploadForm.actionFlag.value = "catsystem";
	uploadForm.enctype = "multipart/form-data";
	uploadForm.submit();
}

//-----------------------------------------------
function actionDelete(id, name){
	if(!confirm("Deleting category: " + name)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goEdit(id){
	var form = document.goForm;
	form.action = "category,custom_edit.jsp";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">
<input type="hidden" name="originPage" value="<%= getPageName()%>">

<div class="title">Custom Category</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Name</td>
		<td>
			<input type="text" name="name" size="25">
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50", maxlength="50"></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="EXPORT" onclick="javascript:actionExport();">
<input type="button" value="IMPORT" onclick="javascript:actionImport(this.form);">
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
<div class="div-upload">
<button>Select a file...</button>
<input type="file" name="file1">
</div>
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="4"></td></tr>
	<tr class="head">
		<td width="200">Name</td>
		<td width="300">Description</td>
		<td width="">Domain</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="4"></td></tr>

<%
List<CategoryData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='4' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	CategoryData data = dataList.get(i);

	String name = data.name;
	if(data.systemFlag){
		name = "*" + name;
	}

	int domainCnt = data.getDomainCount();
	if(domainCnt > 0){
		name = name + " - " + domainCnt;
	}

	String domainLine = data.getDomainLine();
	if(domainLine.length() > 100){
		domainLine = domainLine.substring(0, 100) + "..";
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='4'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= name%></td>
		<td><%= data.description%></td>
		<td><%= domainLine%></td>

		<td align="right">
		<input type="button" value="EDIT" onclick="javascript:goEdit(<%= data.id%>)">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.name%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="4"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
