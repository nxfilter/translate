<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
boolean checkParam(DnsSetupData data){
	if (!isValidIp(data.upstreamDns1)) {
		errList.add("Invalid IP address for DNS server #1.");
		return false;
	}

	if (isNotEmpty(data.upstreamDns2) && !isValidIp(data.upstreamDns2)) {
		errList.add("Invalid IP address for DNS server #2.");
		return false;
	}

	if (isNotEmpty(data.upstreamDns3) && !isValidIp(data.upstreamDns3)) {
	    errList.add("Invalid IP address for DNS server #3.");
	    return false;
	}

	return true;
}

//-----------------------------------------------
void update(DnsSetupDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	DnsSetupData data = dao.selectOne();

	// DNS setup.	
	data.upstreamDns1 = paramString("upstreamDns1");
    data.upstreamDns2 = paramString("upstreamDns2");
    data.upstreamDns3 = paramString("upstreamDns3");
    data.upstreamTimeout = paramInt("upstreamTimeout");
    data.respCacheSize = paramInt("respCacheSize");
	data.usePersistentCache = paramBoolean("usePersistentCache");
	data.useNegativeCache = paramBoolean("useNegativeCache");
	data.minimalResponses = paramBoolean("minimalResponses");

    data.minCacheTtl = paramInt("minCacheTtl");
    data.blockCacheTtl = paramInt("blockCacheTtl");

    data.localDns = paramString("localDns");
    data.localDomain = paramString("localDomain");
    data.localTimeout = paramInt("localTimeout");
	data.useLocalDns = paramBoolean("useLocalDns");

	data.dropHostnameWithoutDomain = paramBoolean("dropHostnameWithoutDomain");
	data.dropPtrForPrivateIp = paramBoolean("dropPtrForPrivateIp");
	data.allowPtrForServerIp = paramBoolean("allowPtrForServerIp");

	// DNS Over HTTPS.
    data.httpsDnsType = paramInt("httpsDnsType");
    data.httpsDnsTimeout = paramInt("httpsDnsTimeout");
	data.failsafeWithUdp53 = paramBoolean("failsafeWithUdp53");
	data.useHttpsDns = paramBoolean("useHttpsDns");

	// Validate and update it.
	if(checkParam(data) && dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
DnsSetupDao dao = new DnsSetupDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
DnsSetupData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">DNS Setup</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Upstream DNS Server #1</td>
		<td><input type="text" name="upstreamDns1" value="<%= data.upstreamDns1%>" size="25"></td>
	</tr>
	
	<tr>
		<td>Upstream DNS Server #2</td>
		<td><input type="text" name="upstreamDns2" value="<%= data.upstreamDns2%>" size="25"></td>
	</tr>

	<tr>
		<td>Upstream DNS Server #3</td>
		<td><input type="text" name="upstreamDns3" value="<%= data.upstreamDns3%>" size="25"></td>
	</tr>
	
	<tr>
		<td>Upstream DNS Query Timeout</td>
		<td><input type="text" name="upstreamTimeout" size="2" maxlength="2" value="<%= data.upstreamTimeout%>"> seconds, 1 ~ 20</td>
	</tr>

<%
DecimalFormat dfmt = new DecimalFormat("###,###");
String size1 = dfmt.format(data.inMemoryCacheSize);
String size2 = dfmt.format(data.persistentCacheSize);
String size3 = dfmt.format(data.negativeCacheSize);
%>
	<tr>
		<td>Response Cache Size</td>
		<td><input type="text" name="respCacheSize" size="7" maxlength="8"
			value="<%= data.respCacheSize%>"> 100,000 ~ 10,000,000<br>
			Current Size : In-memory = <%= size1%> / Persistent = <%= size2%> / Negative = <%= size3%>
		</td>
	</tr>

	<tr>
		<td>Use Persistent Cache</td>
		<td><input type="checkbox" class="no-border"
			name="usePersistentCache" <%if(data.usePersistentCache){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Use Negative Cache</td>
		<td><input type="checkbox" class="no-border"
			name="useNegativeCache" <%if(data.useNegativeCache){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Minimal Responses</td>
		<td><input type="checkbox" class="no-border"
			name="minimalResponses" <%if(data.minimalResponses){out.print("checked");}%>></td>
	</tr>
	
	<tr>
		<td>Minimum Cache TTL</td>
		<td><input type="text" name="minCacheTtl" size="2" maxlength="4" value="<%= data.minCacheTtl%>"> seconds, 0 ~ 3600, 0 = Bypass</td>
	</tr>
	
	<tr>
		<td>Block Cache TTL</td>
		<td><input type="text" name="blockCacheTtl" size="2" maxlength="4" value="<%= data.blockCacheTtl%>"> seconds, 0 ~ 3600</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Local DNS</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Local DNS Server</td>
		<td>
		Multiple IP addresses should be separated by commas.<br>
		<input type="text" name="localDns" value="<%= data.localDns%>" size="25">
		</td>
	</tr>

	<tr>
		<td>Local Domain</td>
		<td>
		Multiple domains should be separated by commas.<br>
		<input type="text" name="localDomain" value="<%= data.localDomain%>" size="50">
		</td>
	</tr>
	
	<tr>
		<td>Local DNS Query Timeout</td>
		<td><input type="text" name="localTimeout" size="2" maxlength="2" value="<%= data.localTimeout%>"> seconds, 1 ~ 20</td>
	</tr>

	<tr>
		<td>Use Local DNS</td>
		<td><input type="checkbox" class="no-border"
			name="useLocalDns" <%if(data.useLocalDns){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">DNS Over HTTPS</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">HTTPS DNS Server</td>
		<td>
			<input type="radio" class="no-border" name="httpsDnsType" value="1" <%if(data.httpsDnsType == 1){out.print("checked");}%>> Cloudflare
			<input type="radio" class="no-border" name="httpsDnsType" value="2" <%if(data.httpsDnsType == 2){out.print("checked");}%>> Google
		</td>
	</tr>
	
	<tr>
		<td>HTTPS DNS Query Timeout</td>
		<td><input type="text" name="httpsDnsTimeout" size="2" maxlength="2" value="<%= data.httpsDnsTimeout%>"> seconds, 1 ~ 20</td>
	</tr>

	<tr>
		<td>Fail-safe With UDP/53</td>
		<td><input type="checkbox" class="no-border"
			name="failsafeWithUdp53" <%if(data.failsafeWithUdp53){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Use HTTPS DNS</td>
		<td><input type="checkbox" class="no-border"
			name="useHttpsDns" <%if(data.useHttpsDns){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Misc</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="250">Drop Hostname Without Domain</td>
		<td><input type="checkbox" class="no-border"
			name="dropHostnameWithoutDomain" <%if(data.dropHostnameWithoutDomain){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Drop Reverse Lookup For Private IP</td>
		<td><input type="checkbox" class="no-border"
			name="dropPtrForPrivateIp" <%if(data.dropPtrForPrivateIp){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Allow Reverse Lookup For Server IP</td>
		<td><input type="checkbox" class="no-border"
			name="allowPtrForServerIp" <%if(data.allowPtrForServerIp){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<%@include file="include/bottom.jsp"%>
