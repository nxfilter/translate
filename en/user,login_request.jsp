<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void approve(LoginRequestDao dao){
	LoginRequestData data = new LoginRequestData();
	data.id = paramInt("id");
	data.uname = paramString("uname");
	data.policyId = paramInt("policyId");
	data.ftPolicyId = paramInt("ftPolicyId");
	data.grpId = paramInt("grpId");

	if (ParamTest.isDupUser(data.uname)) {
		errList.add("User already exists.");
		return;
	}

	if(dao.approve(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
LoginRequestDao dao = new LoginRequestDao();
dao.page = paramInt("page", 1);
dao.addKw(paramString("kw"));

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("approve")){
	approve(dao);
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gKw = paramString("kw");

// Get policy and group list.
List<PolicyData> gPolicyList = new PolicyDao().selectList();
List<GroupData> gGroupList = new GroupDao().selectListUserCreatedOnly();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionApprove(id, uname){
	if(!confirm("Approve the login request from " + uname)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "approve";
	form.id.value = id;
	form.uname.value = uname;

	form.policyId.value = document.getElementById("policy_" + id).value;
	form.ftPolicyId.value = document.getElementById("ftPolicy_" + id).value;
	form.grpId.value = document.getElementById("grp_" + id).value;

	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function goSearch(kw){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.kw.value = document.all("searchKw").value;
	form.page.value = "1";
	form.submit();
}
</script>

<!-- view -->
<div class="title">Login Request</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>
This feature requires CxLogon that is a single sign-on agent for NxFilter. When you install CxLogon on a user PC, it will try to create a login session<br>
on NxFilter with the logged-in username on the system it is running on. When there's no matching username on NxFilter, it will create a login request<br>
and you can approve the request and create a new user on NxFilter.
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td width="50%">
			Count : <%= gCount%> / Page : <%= gPage%>
		</td>
		<td align="right">
			<input type="text" name="searchKw" size="25" value="<%= gKw%>"
				onkeypress="javascript:if(event.keyCode == 13){goSearch(); return;}">
			<input type="button" value="SEARCH" onclick="javascript:goSearch()">
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="8"></td></tr>
	<tr class="head">
		<td width="100">Time</td>
		<td width="200">Username</td>
		<td width="150">Client IP</td>
		<td width="">OS</td>
		<td width="200">Work-time Policy</td>
		<td width="200">Free-time Policy</td>
		<td width="200">Group</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="8"></td></tr>

<%
List<LoginRequestData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='8' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	LoginRequestData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='8'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td><%= data.uname%></td>
		<td><%= data.cltIp%></td>
		<td><%= data.getOsname()%></td>

		<td>
<select id="policy_<%= data.id%>">
	<option value="0">---------------------------
<%
for(PolicyData policy : gPolicyList){
	printf("<option value='%s'>%s", policy.id, policy.name);
}
%>
</select>
		</td>

		<td>
<select id="ftPolicy_<%= data.id%>">
	<option value="0">---------------------------
<%
for(PolicyData policy : gPolicyList){
	printf("<option value='%s'>%s", policy.id, policy.name);
}
%>
</select>
		</td>

		<td>
<select id="grp_<%= data.id%>">
	<option value="0">---------------------------
<%
for(GroupData grp : gGroupList){
	printf("<option value='%s'>%s", grp.id, grp.name);
}
%>
</select>
		</td>

		<td align="right">
			<input type="button" value="APPROVE" onclick="javascript:actionApprove(<%= data.id%>, '<%= data.uname%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="8"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="kw" value="<%= gKw%>">
<input type="hidden" name="id" value="">
<input type="hidden" name="uname" value="">
<input type="hidden" name="policyId" value="">
<input type="hidden" name="ftPolicyId" value="">
<input type="hidden" name="grpId" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
