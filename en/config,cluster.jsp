<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(ClusterDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	ClusterData data = new ClusterData();
	data.clusterMode = paramInt("clusterMode");
	data.masterIp = paramString("masterIp");
	data.slaveIp = paramString("slaveIp");

	if(dao.update(data)){
		succList.add("Restarting needed to apply new settings.");
	}
}

//-----------------------------------------------
void delete(ClusterDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ClusterDao dao = new ClusterDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

// Global.
ClusterData data = dao.selectOne();
int gNodeCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function set_clusterMode(form){
	var val = radioGetValue(form.clusterMode);

	if(val == 0){
		form.masterIp.value = "";
		form.masterIp.disabled = true;

		form.slaveIp.value = "";
		form.slaveIp.disabled = true;
	}
	else if(val == 1){
		form.masterIp.value = "";
		form.masterIp.disabled = true;

		form.slaveIp.disabled = false;
	}
	else if(val == 2){
		form.slaveIp.value = "";
		form.slaveIp.disabled = true;

		form.masterIp.disabled = false;
	}
}

//-----------------------------------------------
function actionDelete(id, nodeIp){
	if(!confirm("Deleting node state-info : " + nodeIp)){
		return;
	}

	var form = document.goForm;
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}
</script>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">Cluster</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Mode</td>
		<td>
			<input type="radio" class="no-border" name="clusterMode" value="0" onclick="javascript:set_clusterMode(this.form)"
				<%if(data.clusterMode == 0){out.print("checked");}%>> None
			<input type="radio" class="no-border" name="clusterMode" value="1" onclick="javascript:set_clusterMode(this.form)"
				<%if(data.clusterMode == 1){out.print("checked");}%>> Master
			<input type="radio" class="no-border" name="clusterMode" value="2" onclick="javascript:set_clusterMode(this.form)"
				<%if(data.clusterMode == 2){out.print("checked");}%>> Slave
		</td>
	</tr>

	<tr>
		<td>Master IP</td>
		<td>
			If it is a slave node it needs to have a master node.<br>
			<input type="text" name="masterIp" value="<%= data.masterIp%>" size="25">
		</td>
	</tr>

	<tr>
		<td>Slave IP</td>
		<td>
			Add slave node IP addresses to be allowed in your cluster. You can add up to 4 IP addresses<br>
			separated by commas.<br>
			<textarea name="slaveIp" cols="80" rows="2"><%= data.slaveIp%></textarea>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<script type="text/javascript">
set_clusterMode(document.forms[0]);
</script>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gNodeCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="7"></td></tr>
	<tr class="head">
		<td width="200">Node</td>
		<td width="200">Last Contact</td>
		<td width="100">Request</td>
		<td width="100">Block</td>
		<td width="100">User</td>
		<td width="">Client IP</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="7"></td></tr>

<%
List<NodeData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='7' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	NodeData nd = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='7'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= nd.nodeIp%></td>
		<td><%= nd.getAtime()%></td>
		<td><%= nd.reqCnt%></td>
		<td><%= nd.blockCnt%></td>
		<td><%= nd.userCnt%></td>
		<td><%= nd.cltIpCnt%></td>
		<td align="right">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= nd.id%>, '<%= nd.nodeIp%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="7"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
