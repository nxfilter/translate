<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(BlocklistDao dao){
	BlocklistData data = new BlocklistData();
	
	data.id = paramInt("id");
	data.priority = paramInt("priority");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void insert(BlocklistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	BlocklistData data = new BlocklistData();
	
	data.url = paramString("url");
	data.categoryId = paramInt("categoryId");
	data.priority = paramInt("priority");

	// Param validation.
    if(isEmpty(data.url) || !ParamTest.isValidBlocklistUrl(data.url)){
		errList.add("Invalid URL.");
		return;
	}

    if(data.categoryId == 0){
		errList.add("Invalid category.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(BlocklistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void test(BlocklistDao dao){
	try{
		dao.test(paramInt("id"));
		succList.add("Blocklist URL connection succeeded.");
	}
	catch(Exception e){
		errList.add(e.toString());
	}
}

//-----------------------------------------------
void merge(BlocklistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(!dao.merge(paramInt("id"))){
		errList.add("We already have a working thread.");
		return;
	}
	succList.add("A worker thread started. Depending on the blocklist size, it will take some time to finish the job.");
}

//-----------------------------------------------
void mergeAll(BlocklistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(!dao.mergeAll()){
		errList.add("We already have a working thread.");
		return;
	}
	succList.add("A worker thread started. Depending on the blocklist size, it will take some time to finish the job.");
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
BlocklistDao dao = new BlocklistDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("test")){
	test(dao);
}
if(actionFlag.equals("merge")){
	merge(dao);
}
if(actionFlag.equals("mergeAll")){
	mergeAll(dao);
}

//
ActiveThreadData atd = dao.getActiveThread();
if(atd != null){
	infoList.add("We are working on " + atd.url + " from " + atd.getStime() + ", line count = " + atd.lineCnt +
		". You can't start another theread before we finish the current job.");
}

// Global.
int gCount = dao.selectCount();
String gKw = paramString("kw");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionUpdate(id){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "update";
	form.id.value = id;
	form.priority.value = $("#priority_" + id).val();
	form.submit();
}

//-----------------------------------------------
function actionDelete(id, url){
	if(!confirm("Deleting URL : " + url)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionTest(id){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "test";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionMerge(id){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "merge";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionMergeAll(){
	if(!confirm("Starting a worker thread for merging all the blocklists?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "mergeAll";
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">
<input type="hidden" name="originPage" value="<%= getPageName()%>">

<div class="title">Blocklist</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
You can download and merge the blocklists from the Internet into Jahaslist and Globlist. When you add a blocklist URL here,
it will be processed overnight.
		</td>
	</tr>

	<tr>
		<td width="200">URL</td>
		<td>
			<input type="text" name="url" size="100">
		</td>
	</tr>

	<tr>
		<td>Category</td>
		<td>
<select name="categoryId">
<option value="0">Select a category
<%
Map<Integer,String> jahasCategoryMap = dao.getJahasCategoryMap();
for(Map.Entry<Integer,String> entry : jahasCategoryMap.entrySet()){
	int categoryId = entry.getKey();
	String categoryName = entry.getValue();
	printf("<option value='%s'>%s", categoryId, categoryName);
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Priority Points</td>
		<td>
		<div style="margin-bottom:5px;">Blocklist with Higher priority points will be processed before others.</div>
			<input type="text" name="priority" value="0" size="3"> -1 ~ 1000, -1 = Exclude from overnight processing.
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="MERGE ALL" onclick="javascript:actionMergeAll();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">
</form>

<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="5"></td></tr>
	<tr class="head">
		<td width="650">URL</td>
		<td width="150">Category</td>
		<td width="150">Priority Points</td>
		<td width="">Merged</td>
		<td width="300"></td>
	</tr>
	<tr class="line"><td colspan="5"></td></tr>

<%
List<BlocklistData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='5' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	BlocklistData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='5'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.url%></td>
		<td><%= data.categoryName%></td>
		<td><input type="text" id="priority_<%= data.id%>" size="2" value="<%= data.priority%>" style="text-align:right;"></td>
		<td><%= data.mergeCnt%></td>
		<td align="right">
			<input type="button" value="TEST" onclick="javascript:actionTest(<%= data.id%>)">
			<input type="button" value="MERGE" onclick="javascript:actionMerge(<%= data.id%>)">
			<input type="button" value="UPDATE" onclick="javascript:actionUpdate(<%= data.id%>)">
			<input type="button" value="DELETE" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.url%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="5"></td></tr>
</table>

</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
<input type="hidden" name="priority" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
