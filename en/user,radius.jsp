<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(RadiusDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	RadiusData data = new RadiusData();

    data.radiusAcctPort = paramInt("radiusAcctPort");
    data.radiusSharedSecret = paramString("radiusSharedSecret");
    data.radiusEnableLogout = paramBoolean("radiusEnableLogout");
	data.useRadius = paramBoolean("useRadius");

	// Validate and update it.
	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
RadiusDao dao = new RadiusDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
RadiusData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">RADIUS</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height: 20px">
You need to restart NxFilter after changing RADIUS setup.
</div>
		</td>
	</tr>

	<tr>
		<td width="210">Accounting Port</td>
		<td><input type="text" name="radiusAcctPort" value="<%= data.radiusAcctPort%>" size="2"></td>
	</tr>

	<tr>
		<td>Shared Secret</td>
		<td><input type="text" name="radiusSharedSecret" value="<%= data.radiusSharedSecret%>" size="25"></td>
	</tr>

	<tr>
		<td>Enable Logout</td>
		<td><input type="checkbox" class="no-border"
			name="radiusEnableLogout" <%if(data.radiusEnableLogout){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Use Radius</td>
		<td><input type="checkbox" class="no-border"
			name="useRadius" <%if(data.useRadius){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<%@include file="include/bottom.jsp"%>
