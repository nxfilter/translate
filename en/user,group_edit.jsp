<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(GroupDao dao){
	GroupData data = new GroupData();
	data.id = paramInt("id");
	data.policyId = paramInt("policyId");
	data.ftPolicyId = paramInt("ftPolicyId");
	data.description = paramString("description");

	String stimeHh = paramString("stimeHh");
	String stimeMm = paramString("stimeMm");
	String etimeHh = paramString("etimeHh");
	String etimeMm = paramString("etimeMm");

	data.ftStime = stimeHh + stimeMm;
	data.ftEtime = etimeHh + etimeMm;

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void moveUser(GroupDao dao){
	if(dao.moveUser(paramInt("id"), paramString("userIdLine"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
GroupDao dao = new GroupDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("moveUser")){
	moveUser(dao);
}

// Global.
GroupData data = dao.selectOne(paramInt("id"));

// Get policy list.
List<PolicyData> gPolicyList = new PolicyDao().selectList();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionMoveUser(form){
	form.actionFlag.value = "moveUser";
	form.userIdLine.value = listboxGetAllValues("dstBox");
	form.submit();
}

//-----------------------------------------------
function listboxGetAllValues(listID){
	var listbox = document.getElementById(listID);

	var idLine = "";
	for(var i = 0; i < listbox.options.length; i++) {
		if(i > 0){
			idLine += ",";
		}
		idLine += listbox.options[i].value;
	}
	return idLine;
}

//-----------------------------------------------
function listboxMoveAcross(sourceID, destID){
	var src = document.getElementById(sourceID);
	var dest = document.getElementById(destID);

	for(var count = 0; count < src.options.length; count++){
		if(src.options[count].selected == true){
			var option = src.options[count];

			var newOption = document.createElement("option");
			newOption.value = option.value;
			newOption.text = option.text;
			newOption.selected = true;
			try{
				dest.add(newOption, null); //Standard
				src.remove(count, null);
			}
			catch(error){
				dest.add(newOption); // IE only
				src.remove(count);
			}
			count--;
		}
	}
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">
<input type="hidden" name="id" value="<%= data.id%>">
<input type="hidden" name="userIdLine" value="">

<!--  -->
<div class="title">Group</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Name</td>
		<td>
			<%= data.name%>
		</td>
	</tr>

	<tr>
		<td>Work-time Policy</td>
		<td>
<select name="policyId">
<%
for(PolicyData pd : gPolicyList){
	if(pd.id == data.policyId){
		printf("<option value='%s' selected>%s</option>\n", pd.id, pd.name);
	}
	else{
		printf("<option value='%s'>%s</option>\n", pd.id, pd.name);
	}
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Free-time Policy</td>
		<td>
<select name="ftPolicyId">
	<option value="0">Same as work-time policy
<%
for(PolicyData pd : gPolicyList){
	if(pd.id == data.ftPolicyId){
		printf("<option value='%s' selected>%s</option>\n", pd.id, pd.name);
	}
	else{
		printf("<option value='%s'>%s</option>\n", pd.id, pd.name);
	}
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Group Specific Free-time</td>
		<td>
		
<%
List<String> hhList = getHhList();
List<String> mmList = getMmList();
%>
<select name="stimeHh">
<%
for(String hh : hhList){
	if(data.ftStime.startsWith(hh)){
		printf("<option value='%s' selected>%s", hh, hh);
	}
	else{
		printf("<option value='%s'>%s", hh, hh);
	}
}
%>
</select>

<select name="stimeMm">
<%
for(String mm : mmList){
	if(data.ftStime.endsWith(mm)){
		printf("<option value='%s' selected>%s", mm, mm);
	}
	else{
		printf("<option value='%s'>%s", mm, mm);
	}
}
%>
</select>
 ~
<select name="etimeHh">
<%
for(String hh : hhList){
	if(data.ftEtime.startsWith(hh)){
		printf("<option value='%s' selected>%s", hh, hh);
	}
	else{
		printf("<option value='%s'>%s", hh, hh);
	}
}
%>
</select>

<select name="etimeMm">
<%
for(String mm : mmList){
	if(data.ftEtime.endsWith(mm)){
		printf("<option value='%s' selected>%s", mm, mm);
	}
	else{
		printf("<option value='%s'>%s", mm, mm);
	}
}
%>
</select>
		
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50" value="<%= data.description%>"></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

<%if(!data.isLdapGroup()){%>

<!--  -->
<div class="title">Move user</div>

<!-- user group move -->
<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

<tr>
	<td width="200">
	<div style="margin-bottom:5px;">All User:</div>
<select id="srcBox" size="25" multiple style="width: 200px;">
<%
for(GroupUserRelationData rd : data.groupUserRelationList){
	printf("<option value='%s'>%s", rd.userId, rd.asString());
}
%>
</select>
	</td>
	
	<td width="50">
<div style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;">
<input type="button" value="&nbsp;>>&nbsp;" onclick="javascript:listboxMoveAcross('srcBox', 'dstBox')">
</div>
<div style="margin-left: 5px; margin-right: 5px; margin-bottom: 5px;">
<input type="button" value="&nbsp;<<&nbsp;" onclick="javascript:listboxMoveAcross('dstBox', 'srcBox')">
</div>
	</td>
	
	<td>
	<div style="margin-bottom:5px;">Member User:</div>
<select id="dstBox" size="25" multiple style="width: 200px;">
<%
for(UserData ud : data.userList){
	printf("<option value='%s'>%s", ud.id, ud.name);
}
%>
</select>
	</td>
</tr>

	<tr height="30">
		<td></td>
		<td colspan="2">
<input type="button" value="MOVE USER" onclick="javascript:actionMoveUser(this.form);">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<%}%>

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
