<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(RedirectionDao dao){
	RedirectionData data = new RedirectionData();
	data.src = paramString("src");
	data.dst = paramString("dst");

	if (isEmpty(data.src)) {
		errList.add("Source domain missing.");
		return;
	}

	if (isEmpty(data.dst) || data.dst.indexOf('.') == -1) {
		errList.add("Invalid IP or domain.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(RedirectionDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
RedirectionDao dao = new RedirectionDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

// Global.
int gCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, domain){
	if(!confirm("Deleting domain : " + domain)){
		return;
	}

	var form = document.goForm;
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">Redirection</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Domain</td>
		<td>
			<input type="text" name="src" size="25">
		</td>
	</tr>

	<tr>
		<td>IP / Domain</td>
		<td>
			<input type="text" name="dst" size="25">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="3"></td></tr>
	<tr class="head">
		<td width="300">Domain</td>
		<td width="">IP / Domain</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="3"></td></tr>

<%
List<RedirectionData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='3' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	RedirectionData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='3'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.src%></td>
		<td><%= data.dst%></td>
		<td align="right">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.src%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="3"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
