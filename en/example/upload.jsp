<%@include file="../include/lib.jsp"%>
<%@page import="com.oreilly.servlet.MultipartRequest"%>
<%@page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%
// Only localhost access allowed.
if(!request.getRemoteAddr().startsWith("127.0.0.1")){
	out.println(request.getRemoteAddr());
	return;
}

int maxSize  = 1024 * 1024 * 10;
String uploadPath = GlobalDao.getWwwTmpPath();
try{
	MultipartRequest mreq = new MultipartRequest(request, uploadPath, maxSize, "UTF-8", new DefaultFileRenamePolicy());

	// File name.
	String file1 = mreq.getFilesystemName("file1");

	// If we have an uploaded file.
	if(isNotEmpty(file1)){

		// Full path to the file.
		String filepath = GlobalDao.getWwwTmpPath() + "/" + file1;
		out.println(filepath);
	}
}
catch(Exception e){
//	e.printStackTrace();
}
%>
<html>
<body>
 <form action='<%= getPageName()%>' method='post' enctype='multipart/form-data'>
	<input type='file' name='file1'><br>
	<input type='submit'>
</form>
</body>
</html>
