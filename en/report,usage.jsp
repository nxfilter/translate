<%@include file="include/top.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();
permission.addReport();

//Check permission.
if(!checkPermission()){
	return;
}

// Global.
String gUser = paramString("user");
String gEtime = strftimeAdd("yyyy/MM/dd", 86400 * -1);
String gStime = strftimeAdd("yyyy/MM/dd", 86400 * -30);
%>

<script type="text/javascript">
//-----------------------------------------------
function setUserdef(form){
	form.timeOption[0].checked = true;
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="get">
<div class="title">Usage Report</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="100" align="right">
	User :
		</td>
		<td>
			<input type="text" value="<%= gUser%>" name="user" size="25">
			<select onchange="javascript:this.form.user.value=this.value">
			<option value=''> Select a user
<%
List<String> userList = new D1ReportDao("20000101", "").getLogUserList();
for(String uname : userList){
	printf("<option value='%s'>%s", uname, uname);
}
%>
			</select>
		</td>

	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<div class="title">
<%= gStime%> ~ <%= gEtime%>
<%
if(isNotEmpty(gUser)){
	out.print(", " + gUser);
}
%>
</div>
</p>

<!-- list -->
<div class="list">

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="9"></td></tr>
	<tr class="head">
		<td width="90">Time</td>
		<td width="110" align="right">Request Sum</td>
		<td width="110" align="right">Request Count</td>
		<td width="110" align="right">Block Sum</td>
		<td width="110" align="right">Block Count</td>
		<td width="110" align="right">Domain Count</td>
		<td width="110" align="right">User Count</td>
		<td width="110" align="right">Client IP</td>
		<td width=""></td>
	</tr>
	<tr class="line"><td colspan="9"></td></tr>
<%
for(int i = 0; i < 30; i++){
	String stime = strftimeAdd("yyyyMMdd", (86400 * i * -1) - 1);

	D1ReportDao dao = new D1ReportDao(stime, gUser);
	ReportStatsData stats = dao.getStats();

	if(i > 0){
		out.println("<tr class='line2'><td colspan='9'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= dao.getStime()%></td>
		<td align="right"><%= stats.reqSum%>&nbsp;</td>
		<td align="right"><%= stats.reqCnt%>&nbsp;</td>
		<td align="right"><%= stats.blockSum%>&nbsp;</td>
		<td align="right"><%= stats.blockCnt%>&nbsp;</td>
		<td align="right"><%= stats.domainCnt%>&nbsp;</td>
		<td align="right"><%= stats.userCnt%>&nbsp;</td>
		<td align="right"><%= stats.cltIpCnt%>&nbsp;</td>
		<td width=""></td>
	</tr>
<%}%>

	<tr class="line"><td colspan="9"></td></tr>
</table>

</div>
<!-- /list -->

<%@include file="include/bottom.jsp"%>
