<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(AdapDao dao){
	LdapData data = new LdapData();
	data.host = paramString("host");
	data.admin = paramString("admin");
	data.passwd = paramString("passwd");
	data.basedn = paramString("basedn");
	data.domain = paramString("domain");
	data.period = paramInt("period");

	// Param validation.
	if (!isValidIp(data.host)) {
		errList.add("Invalid host IP.");
		return;
	}

	if (isEmpty(data.admin)) {
		errList.add("Admin missing.");
		return;
	}

	if (isEmpty(data.basedn)) {
		errList.add("Base DN missing.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(AdapDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void test(AdapDao dao){
	try{
		dao.test(paramInt("id"));
		succList.add("LDAP connection succeeded.");
	}
	catch(Exception e){
		errList.add(e.toString());
	}
}

//-----------------------------------------------
void importLdap(AdapDao dao){
	String res = dao.importLdap(paramInt("id"));
	if(res == null){
		errList.add("LDAP import failed.");
	}
	else{
		succList.add("LDAP imported.");
		succList.add(res);
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
AdapDao dao = new AdapDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("test")){
	test(dao);
}
if(actionFlag.equals("importLdap")){
	importLdap(dao);
}

// Global.
int gCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, host){
	if(!confirm("Deleting host : " + host
		+ "\nAll the users and groups associated to the host will be lost.")){

		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goEdit(id){
	var form = document.goForm;
	form.action = "user,adap_edit.jsp";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionTest(id){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "test";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionImport(id){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "importLdap";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">Active Directory</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="200">Host</td>
		<td>
			<input type="text" name="host" size="25"> ex) 192.168.0.100
		</td>
	</tr>

	<tr>
		<td>Admin</td>
		<td>
			<input type="text" name="admin" size="25"> ex) Administrator@nxfilter.local
		</td>
	</tr>

	<tr>
		<td>Password</td>
		<td>
			<input type="password" name="passwd" size="25">
		</td>
	</tr>


	<tr>
		<td>Base DN</td>
		<td>
			<input type="text" name="basedn" size="40"> ex) cn=users,dc=nxfilter,dc=local
		</td>
	</tr>

	<tr>
		<td>Domain</td>
		<td>
			<input type="text" name="domain" size="40"> ex) nxfilter.local
		</td>
	</tr>

	<tr>
		<td>Auto-sync</td>
		<td>

<select name="period">
<%
Map<Integer, String> periodMap = getLdapPeriodMap();
for(Map.Entry<Integer, String> entry : periodMap.entrySet()){
	Integer key = entry.getKey();
	String val = entry.getValue();

	printf("<option value='%s'>%s", key, val);
}
%>
</select>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<!-- view -->

<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="7"></td></tr>
	<tr class="head">
		<td width="120">Host</td>
		<td width="250">Admin</td>
		<td width="300">Base DN</td>
		<td width="120">Domain</td>
		<td width="150" align="center">Follow Referral</td>
		<td width="120">Auto-sync</td>
		<td width=""></td>
	</tr>
	<tr class="line"><td colspan="7"></td></tr>

<%
List<LdapData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='7' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	LdapData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='7'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.host%></td>
		<td><%= data.admin%></td>
		<td><%= data.basedn%></td>
		<td><%= data.domain%></td>
		<td align="center"><%= data.getFollowReferralYn()%></td>
		<td><%= getLdapPeriodStr(data.period)%></td>
		<td align="right">
		<input type="button" value="IMPORT" onclick="javascript:actionImport(<%= data.id%>)">
		<input type="button" value="TEST" onclick="javascript:actionTest(<%= data.id%>)">
		<input type="button" value="EDIT" onclick="javascript:goEdit(<%= data.id%>)">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.host%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="7"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
