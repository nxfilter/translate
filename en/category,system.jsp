<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void delete(CategorySystemDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void deleteAll(CategorySystemDao dao){
	if(dao.deleteAll()){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
boolean hasBlacklist(int blacklistType){
	if(blacklistType != 8 && blacklistType != 99){
		return true;
	}
	return false;
}

//-----------------------------------------------
void update(CategorySystemDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	// License update.
	String licenseKey = paramString("licenseKey");
	if(isNotEmpty(licenseKey)){
		LicenseDao licDao = new LicenseDao();

		if(!licDao.isValidLicenseKey(licenseKey)){
			errList.add("Invalid license key.");
			return;
		}
		
		if(licDao.isExpiredLicenseKey(licenseKey)){
			errList.add("Expired license key.");
			return;
		}

		if(new LicenseDao().updateLicenseKey(licenseKey)){
			succList.add("Data updated.");
			warnList.add("Restarting is needed to apply the change.");
		}
		else{
			errList.add("Invalid license key.");
		}
		return;
	}

	// Change blacklist type.
	int blacklistType = paramInt("blacklistType");
	if(blacklistType == 4 && !dao.hasKomodiaLicense()){
		errList.add("Cloudlist license required.");
		return;
	}

	if(dao.updateBlacklistType(blacklistType)){
        succList.add("Data updated.");
        warnList.add("Restarting is needed to apply the change.");
	}
}

//-----------------------------------------------
void updatePolicyGloblist(PolicyGloblistDao dao){
	PolicyGloblistData data = dao.selectOne();

	data.blockAds = paramBoolean("blockAds");
	data.blockPhimal = paramBoolean("blockPhimal");
	data.blockPorn = paramBoolean("blockPorn");
	data.adRemove = paramBoolean("adRemove");

	// Update it.
	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
CategorySystemDao dao = new CategorySystemDao();
PolicyGloblistDao pgDao = new PolicyGloblistDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("updatePolicyGloblist")){
	updatePolicyGloblist(pgDao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("deleteAll")){
	deleteAll(dao);
}
if(actionFlag.equals("export")){
	String filename = "catsystem_" + strftime("yyyyMMddHHmm") + ".txt";

	if(dao.exportFile(filename)){
		response.sendRedirect("download.jsp?filename=" + filename + "&contentType=text/plain");
		return;
	}
	else{
		errList.add("Couldn't write the file.");
	}
}

// If it's about importation.
int importCount = paramInt("importCount");
if(importCount > 0){
	if(actionFlag.equals("ruleset")){
		succList.add(importCount + " classification rules imported.");
	}
	else{
		succList.add(importCount + " domains imported.");
	}
}

// Global.
int gCount = dao.selectCount();

// 4 = Cloudlist, 5 = Jahaslist, 8 = Globlist, 99 = None.
int gBlacklistType = dao.getBlacklistType();
String gLicenseEndDate = dao.getLicenseEndDate();
int gLicenseMaxUser = dao.getLicenseMaxUser();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionExport(){
	var form = document.goForm;
	form.actionFlag.value = "export";
	form.submit();
}

//-----------------------------------------------
function actionImport(uploadForm){
	if(uploadForm.file1.value == ""){
		alert("No file selected.");
		return;
	}

	uploadForm.action = "import.jsp";
	uploadForm.actionFlag.value = "catsystem";
	uploadForm.enctype = "multipart/form-data";
	uploadForm.submit();
}

//-----------------------------------------------
function actionDelete(id, name){
	if(!confirm("Deleting custom classified domains from " + name)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionDeleteAll(){
	if(!confirm("Deleting all the custom classified domains.")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "deleteAll";
	form.submit();
}

//-----------------------------------------------
function goEdit(id){
	var form = document.goForm;
	form.action = "category,system_edit.jsp";
	form.id.value = id;
	form.submit();
}
</script>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">
<input type="hidden" name="originPage" value="<%= getPageName()%>">

<!-- view -->
<div class="title">System Category</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height: 20px">
NxFilter supports several domain categorization options. To find out more about these options, read our online tutorial.
</div>
		</td>
	</tr>

	<tr>
		<td width="200">Categorization Type</td>
		<td>
			<input type="radio" class="no-border" name="blacklistType" value="5" <%if(gBlacklistType == 5){out.print("checked");}%>>
			Jahaslist - Auto-classification with NxClassifier.
			<%if(gBlacklistType == 5){%>
				<div class="tab">
				- End date = <%= gLicenseEndDate%>
				<br>
				- Max user = <%= gLicenseMaxUser%>
				</div>
			<%}%>
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
			<input type="radio" class="no-border" name="blacklistType" value="4" <%if(gBlacklistType == 4){out.print("checked");}%>>
			Cloudlist - Cloud based domain categorization service.
			<%if(gBlacklistType == 4){%>
				<div class="tab">
				- End date = <%= gLicenseEndDate%>
				<br>
				- Max user = <%= gLicenseMaxUser%>
				</div>
			<%}%>
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
			<input type="radio" class="no-border" name="blacklistType" value="8" <%if(gBlacklistType == 8){out.print("checked");}%>>
			Globlist - Free domain categorization database applied globally.
		</td>
	</tr>

	<tr>
		<td>License Key</td>
		<td>
			<input type="text" name="licenseKey" size="50">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">

<%if(hasBlacklist(gBlacklistType)){%>
<input type="button" value="DEL-DOMAIN-ALL" onclick="javascript:actionDeleteAll();">
<input type="button" value="EXPORT" onclick="javascript:actionExport();">
<input type="button" value="IMPORT" onclick="javascript:actionImport(this.form);">
<%}%>

		</td>
	</tr>

<%if(hasBlacklist(gBlacklistType)){%>
	<tr>
		<td></td>
		<td>
<div class="div-upload">
<button>Select a file...</button>
<input type="file" name="file1">
</div>
		</td>
	</tr>
<%}%>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<!-- /view -->
<p>

<!-- Globlist Policy -->
<%
if(gBlacklistType == 8){

	PolicyGloblistData pgData = pgDao.selectOne();
	//int pgCount = pgDao.selectCount();
%>
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="updatePolicyGloblist">

<div class="title">Globlist Policy</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height: 20px">
Globlist is a free domain categorization database derived from Jahaslist. It works on global policy level. At the moment, Globlist has more than<br>
400,000 domains classified into 3 categories. It updates itself automatically in background.
</div>
		</td>
	</tr>
	
	<tr>
		<td width="200">Block Ads</td>
		<td><input type="checkbox" class="no-border"
			name="blockAds" <%if(pgData.blockAds){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Block Phishing/Malware</td>
		<td><input type="checkbox" class="no-border"
			name="blockPhimal" <%if(pgData.blockPhimal){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Block Porn</td>
		<td><input type="checkbox" class="no-border"
			name="blockPorn" <%if(pgData.blockPorn){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Ad-remove</td>
		<td><input type="checkbox" class="no-border"
			name="adRemove" <%if(pgData.adRemove){out.print("checked");}%>>
			Block adware with blank-page.
			</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<%}%>
<!-- /Globlist Policy -->

<!-- list -->
<%if(hasBlacklist(gBlacklistType)){%>
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="3"></td></tr>

<%
List<CategoryData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='3' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	CategoryData data = dataList.get(i);

	String name = data.name;
	int domainCnt = data.getDomainCount();
	if(domainCnt > 0){
		name = name + " - " + domainCnt;
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='3'></td></tr>");
	}
%>
	<tr class="row">
		<td width="200"><%= name%></td>
		<td><%= data.description%></td>

		<td width="200" align="right">
		<input type="button" value="ADD-DOMAIN" onclick="javascript:goEdit(<%= data.id%>)">
		<input type="button" value="DEL-DOMAIN" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.name%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="3"></td></tr>
</table>
</div>
<%}%>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
