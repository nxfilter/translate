<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(PolicyDao dao){
	PolicyData data = new PolicyData();

	data.id = paramInt("id");
	data.points = paramInt("points");
	data.description = paramString("description");
	data.enableFilter = paramBoolean("enableFilter");

	data.blockAll = paramBoolean("blockAll");
	data.blockUnclass = paramBoolean("blockUnclass");
	data.adRemove = paramBoolean("adRemove");
	data.maxDomainLen = paramInt("maxDomainLen");

	data.blockCovertChan = paramBoolean("blockCovertChan");
	data.blockMailerWorm = paramBoolean("blockMailerWorm");

	data.aRecordOnly = paramBoolean("aRecordOnly");
	data.quota = paramInt("quota");
	data.quotaAll = paramBoolean("quotaAll");
	data.bwdtLimit = paramLong("bwdtLimit");

	data.safeMode = paramInt("safeMode");
	data.safeModeWithoutYoutube = paramBoolean("safeModeWithoutYoutube");

	data.logOnly = paramBoolean("logOnly");
	data.blockCategoryArr = paramArray("blockCategoryArr");
	data.quotaCategoryArr = paramArray("quotaCategoryArr");

	String stimeHh = paramString("stimeHh");
	String stimeMm = paramString("stimeMm");
	String etimeHh = paramString("etimeHh");
	String etimeMm = paramString("etimeMm");

	data.btStime = stimeHh + stimeMm;
	data.btEtime = etimeHh + etimeMm;

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
PolicyDao dao = new PolicyDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
PolicyData data = dao.selectOne(paramInt("id"));
%>
<%@include file="include/action_info.jsp"%>

<!-- view -->
<form action="<%= getPageName()%>" method="post" name="policy_form">
<input type="hidden" name="actionFlag" value="update">
<input type="hidden" name="id" value="<%= data.id%>">

<!--  -->
<div class="title">Policy</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Name</td>
		<td><%= data.name%></td>
	</tr>

	<tr>
		<td>Priority Points</td>
		<td>
		<div style="margin-bottom:5px;">When a user has multiple policies the policy having the highest points will be applied.</div>
			<input type="text" name="points" value="<%= data.points%>" size="3"> 0 ~ 1000
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td>
		<input type="text" name="description" value="<%= data.description%>" size="50">
		</td>
	</tr>

	<tr>
		<td>Enable Filter</td>
		<td><input type="checkbox" class="no-border" name="enableFilter"
			<%if(data.enableFilter){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Block All</td>
		<td><input type="checkbox" class="no-border" name="blockAll" <%if(data.blockAll){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Block Unclassified</td>
		<td><input type="checkbox" class="no-border" name="blockUnclass" <%if(data.blockUnclass){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Ad-remove</td>
		<td>
		<input type="checkbox" name="adRemove" <%if(data.adRemove){out.print("checked");}%>>
		Block adware with blank-page.
		</td>
	</tr>

	<tr>
		<td>Max Domain Length</td>
		<td>
		NxFilter doesn't apply 'Max Domain Length' against 100,000 most well known domains.<br>
		<input type="text" name="maxDomainLen" value="<%= data.maxDomainLen%>" size="3">
		0 ~ 1000, 0 = bypass
		</td>
	</tr>

	<tr>
		<td>Block Covert Channel</td>
		<td>
		<input type="checkbox" name="blockCovertChan" <%if(data.blockCovertChan){out.print("checked");}%>>
		Detecting hidden communication.
		</td>
	</tr>

	<tr>
		<td>Block Mailer Worm</td>
		<td>
		<input type="checkbox" name="blockMailerWorm" <%if(data.blockMailerWorm){out.print("checked");}%>>
		MX type query will be blocked.
		</td>
	</tr>

	<tr>
		<td>Allow 'A' Record Only</td>
		<td>
		<input type="checkbox" name="aRecordOnly" <%if(data.aRecordOnly){out.print("checked");}%>>
		Block all except A, AAAA, PTR type queries.
		</td>
	</tr>

	<tr>
		<td>Quota</td>
		<td>
		<div style="line-height:20px;">
		You can allow users to use 'Quotaed Categories' for specific amount of time.<br>
		When there's no remaining quota for a user 'Quotaed Categories' will be working in the same way as 'Blocked Categories'.
		</div>
			<input type="text" name="quota" value="<%= data.quota%>" size="3"> 0 ~ 1440 minutes
		</td>
	</tr>

	<tr>
		<td>Quota All</td>
		<td>
		<input type="checkbox" class="no-border" name="quotaAll" <%if(data.quotaAll){out.print("checked");}%>>
		Quota will be applied for all domains including unclassified domains.
		</td>
	</tr>

	<tr>
		<td>Bandwidth Limit</td>
		<td>
		<div style="margin-bottom:5px;">You need to run NetFlow collector to use bandwidth control.</div>
			<input type="text" name="bwdtLimit" value="<%= data.bwdtLimit%>" size="3"> MB, 0 = bypass
		</td>
	</tr>

	<tr>
		<td>Safe Search</td>
		<td>
			<input type="radio" class="no-border" name="safeMode" value="0" <%if(data.safeMode == 0){out.print("checked");}%>> Off
			<input type="radio" class="no-border" name="safeMode" value="1" <%if(data.safeMode == 1){out.print("checked");}%>> Moderate
			<input type="radio" class="no-border" name="safeMode" value="2" <%if(data.safeMode == 2){out.print("checked");}%>> Strict
			<input type="checkbox" name="safeModeWithoutYoutube" <%if(data.safeModeWithoutYoutube){out.print("checked");}%>> Without Youtube
		</td>
	</tr>

	<tr>
		<td>Block-time</td>
		<td>
		
<%
List<String> hhList = getHhList();
List<String> mmList = getMmList();
%>
<select name="stimeHh">
<%
for(String hh : hhList){
	if(data.btStime.startsWith(hh)){
		printf("<option value='%s' selected>%s", hh, hh);
	}
	else{
		printf("<option value='%s'>%s", hh, hh);
	}
}
%>
</select>

<select name="stimeMm">
<%
for(String mm : mmList){
	if(data.btStime.endsWith(mm)){
		printf("<option value='%s' selected>%s", mm, mm);
	}
	else{
		printf("<option value='%s'>%s", mm, mm);
	}
}
%>
</select>
 ~
<select name="etimeHh">
<%
for(String hh : hhList){
	if(data.btEtime.startsWith(hh)){
		printf("<option value='%s' selected>%s", hh, hh);
	}
	else{
		printf("<option value='%s'>%s", hh, hh);
	}
}
%>
</select>

<select name="etimeMm">
<%
for(String mm : mmList){
	if(data.btEtime.endsWith(mm)){
		printf("<option value='%s' selected>%s", mm, mm);
	}
	else{
		printf("<option value='%s'>%s", mm, mm);
	}
}
%>
</select>
		
		</td>
	</tr>

	<tr>
		<td>Logging Only</td>
		<td><input type="checkbox" class="no-border" name="logOnly" <%if(data.logOnly){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Blocked Categories</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>
<input type="button" value="TOGGLE-ALL" onclick="javascript:checkboxToggleAll3('blockCategoryArr');">
	<br>

<%
for(int i = 0; i < data.blockCategoryList.size(); i++){
	CategoryData cd = data.blockCategoryList.get(i);

	String chk = "";
	if(cd.checkFlag){
		chk = "checked";	
	}

	if(i > 0 && i % 6 == 0){
		out.println("<br>");
	}
%>
	<span class="category-item">
<input type="checkbox" class="no-border"
	name="blockCategoryArr" value="<%= cd.id%>" <%= chk%>><%= cd.name%>
	</span>
<%}%>

		</td>
	</tr>

	<tr height="30">
		<td align="center">
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Quotaed Categories</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>
<input type="button" value="TOGGLE-ALL" onclick="javascript:checkboxToggleAll3('quotaCategoryArr');">
	<br>

<%
for(int i = 0; i < data.quotaCategoryList.size(); i++){
	CategoryData cd = data.quotaCategoryList.get(i);

	String chkLine = "";
	if(cd.checkFlag){
		chkLine = "checked";	
	}

	if(i > 0 && i % 6 == 0){
		out.println("<br>");
	}
%>
	<span class="category-item">
		<input type="checkbox" class="no-border" name="quotaCategoryArr" value="<%= cd.id%>" <%= chkLine%>><%= cd.name%>
	</span>
<%}%>

		</td>
	</tr>

	<tr height="30">
		<td align="center">
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
