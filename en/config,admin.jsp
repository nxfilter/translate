<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void updateName(AdminDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	AdminData data = new AdminData();
	data.name = paramString("admin_name");

	// Param validation.
	if(!ParamTest.isValidNameLen(data.name)){
		errList.add(ParamTest.ERR_NAME_LEN);
		return;
	}
	
	if(!ParamTest.isValidNameChar(data.name)){
		errList.add(ParamTest.ERR_NAME_CHAR);
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void updateAdminPw(AdminDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	String newPw = paramString("newPw");
	String newPw2 = paramString("newPw2");
	String adminPw = paramString("adminPw");

	// Validate and update it.
	if(!ParamTest.isValidPasswdLen(newPw)){
		errList.add("Password length must be between 4 and 16.");
		return;
	}
	
	if(!ParamTest.isValidPasswdChar(newPw)){
		errList.add("Only ascii character allowed in password.");
		return;
	}

	if(!dao.isAdminPw(adminPw)){
		errList.add("Wrong password.");
		return;
	}

	if(!newPw.equals(newPw2)){
		errList.add("Confirm password different.");
		return;
	}

	if(dao.updateAdminPw(newPw)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void updateClientPw(AdminDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	String clientPw = paramString("clientPw");

	// Validate and update it.
	if(!ParamTest.isValidPasswdLen(clientPw)){
		errList.add("Password length must be between 4 and 16.");
		return;
	}
	
	if(!ParamTest.isValidPasswdChar(clientPw)){
		errList.add("Only ascii character allowed in password.");
		return;
	}

	if(dao.updateClientPw(clientPw)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void updateReportPw(AdminDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	String reportPw = paramString("reportPw");

	// Validate and update it.
	if(!ParamTest.isValidPasswdLen(reportPw)){
		errList.add("Password length must be between 4 and 16.");
		return;
	}
	
	if(!ParamTest.isValidPasswdChar(reportPw)){
		errList.add("Only ascii character allowed in password.");
		return;
	}

	if(dao.updateReportPw(reportPw)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
AdminDao dao = new AdminDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	updateName(dao);
}
if(actionFlag.equals("adminPw")){
	updateAdminPw(dao);
}
if(actionFlag.equals("clientPw")){
	updateClientPw(dao);
}
if(actionFlag.equals("reportPw")){
	updateReportPw(dao);
}

// Global.
AdminData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<!-- Admin name -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<div class="title">Admin Name</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Admin Name</td>
		<td><input type="text" name="admin_name" value="<%= data.name%>" size="25"></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- / -->

<!-- Admin password -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="adminPw">

<div class="title">Admin Password</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Current Password</td>
		<td><input type="password" name="adminPw" size="25"></td>
	</tr>

	<tr>
		<td>New Password</td>
		<td><input type="password" name="newPw" size="25"></td>
	</tr>

	<tr>
		<td>Confirm Password</td>
		<td><input type="password" name="newPw2" size="25"></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- / -->

<!-- Client password -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="clientPw">

<div class="title">Client Password</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Password</td>
		<td>
		Password for remote filtering client setup.<br>
		<input type="password" name="clientPw" value="<%= data.clientPw%>" size="25"
			onclick="javascript:this.type='text';" onblur="javascript:this.type='password';">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- / -->

<!-- Report password -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="reportPw">

<div class="title">Report Password</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Password</td>
		<td>
		Password for report manager login.<br>
		<input type="password" name="reportPw" value="<%= data.reportPw%>" size="25"
			onclick="javascript:this.type='text';" onblur="javascript:this.type='password';">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- / -->

<%@include file="include/bottom.jsp"%>
