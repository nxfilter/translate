<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void delete(ClassifiedDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void deleteAll(ClassifiedDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.deleteAll()){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void reclassifyAll(ClassifiedDao dao){
	if(dao.reclassifyAll()){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ClassifiedDao dao = new ClassifiedDao();
dao.page = paramInt("page", 1);
dao.addKw(paramString("kw"));

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("deleteAll")){
	deleteAll(dao);
}
if(actionFlag.equals("reclassifyAll")){
	reclassifyAll(dao);
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gKw = paramString("kw");
ClassifiedStatsData stats = dao.getStats();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, domain){
	if(!confirm("Deleting data : " + domain)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionReclassifyAll(){
	if(!confirm("Reclassify all?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "reclassifyAll";
	form.submit();
}

//-----------------------------------------------
function actionDeleteAll(){
	if(!confirm("Delete all?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "deleteAll";
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function goSearch(kw){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.kw.value = document.all("searchKw").value;
	form.page.value = "1";
	form.submit();
}

//-----------------------------------------------
function goView(id){
	var form = document.goForm;
	form.action = "classifier,classified_view.jsp";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goTest(domain){
	var form = document.goForm;
	form.action = "classifier,test_run.jsp";
	form.domain.value = domain;
	form.submit();
}

//-----------------------------------------------
function setPopupForm(domain){
	document.popupForm.domain.value = domain;
	document.popupForm.id.value = 0;
	document.popupForm.chkSubdomain.checked = false;
	$("#popupResult").empty();
}
</script>

<!-- view -->
<div class="title">Classified</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>
When you want to perform reclassification against the already classified data, click 'RECLASSIFY ALL'. It will take some time<br>
if you have many classified data.
		</td>
	</tr>

	<tr>
		<td>
		&nbsp;&nbsp;<input type="button" value="RECLASSIFY ALL"
			onclick="javascript:actionReclassifyAll()">
		<input type="button" value="DELETE ALL"
			onclick="javascript:actionDeleteAll()">
		</td>
	</tr>

	<tr>
		<td>
		Statistics since <%= stats.getCtime()%>,<br>
		Total = <%= stats.totalCount%>
		, Classified = <%= stats.classifiedCount%>
		, Unclassified = <%= stats.unclassifiedCount%>
		, Error = <%= stats.errorCount%>
		, Hit = <%= stats.hitPercentage%>%
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td width="50%">
			Count : <%= gCount%> / Page : <%= gPage%>
		</td>
		<td align="right">
			<input type="text" name="searchKw" size="25" value="<%= gKw%>"
				onkeypress="javascript:if(event.keyCode == 13){goSearch(); return;}">
			<input type="button" value="SEARCH" onclick="javascript:goSearch()">
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="6"></td></tr>
	<tr class="head">
		<td width="100">Time</td>
		<td width="250">Domain</td>
		<td width="">Title</td>
		<td width="150">Category</td>
		<td width="400">Reason</td>
		<td width="150"></td>
	</tr>
	<tr class="line"><td colspan="6"></td></tr>

<%
List<ClassifiedData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='6' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	ClassifiedData data = dataList.get(i);

	String reasonLine = data.reason;
	if(reasonLine.length() > 100){
		reasonLine = safeSubstring(reasonLine, 100) + "..";
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='6'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td>
			<input type="button" value="GO" onclick="javascript:windowOpen('http://' + '<%= data.domain%>')">
			<a href="#recat-popup" class="open-popup-link" title="Click for reclassification"
				onclick="javascript:setPopupForm('<%= data.domain%>')"><%= data.domain%></a>
		</td>
		<td><%= data.title%></td>
		<td title="Click for reclassification"><a href="#recat-popup" class="open-popup-link"
			onclick="javascript:setPopupForm('<%= data.domain%>')"><%= data.categoryName%></a></td>
		<td><%= reasonLine%></td>
		<td align="right">
			<input type="button" value="TEST" onclick="javascript:goTest('<%= data.domain%>')">
			<input type="button" value="VIEW" onclick="javascript:goView(<%= data.id%>)">
			<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.domain%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="6"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="kw" value="<%= gKw%>">
<input type="hidden" name="id" value="">
<input type="hidden" name="domain" value="">
</form>
<!-- /goForm -->

<!-- popupForm -->
<div id="recat-popup" class="xrecat-popup mfp-hide">
	Add domain into a new category.
	</p>

	<form id="popupForm" name="popupForm" method="post" action="category,system_edit.jsp">
	<input type="hidden" name="actionFlag" value="addDomain">
	<table width="100%" cellpadding="2">

		<tr>
			<td>
			<input id="popupFormDomain" type="text" name="domain" size="50"/><br>
			</td>
		</tr>

		<tr>
			<td>
			<select name="id">
			<option value="0"> Select a category to move in
<%
List<CategoryData> catList = new CategorySystemDao().selectList();
for(CategoryData data : catList){
	printf("<option value='%s'> %s", data.id, data.name);
}
%>
			</select>
			</td>
		</tr>

		<tr>
			<td>
			Include subdomains <input id="popupFormChkSubdomain" name="chkSubdomain" type="checkbox"/><br>
			</td>
		</tr>

		<tr>
			<td>
				<input type="submit" value="SUBMIT">
				<input type="button" value="CLOSE" onclick="javascript:$.magnificPopup.close();">
			</td>
		</tr>

	</table>

	</p>
	<div id="popupResult" class="succ-msg"></div>
</div>
<!-- /popupForm -->

<%@include file="include/bottom.jsp"%>

<!-- Popup -->
<script type="text/javascript">
//-----------------------------------------------
$(".open-popup-link").magnificPopup({
	type:"inline",
	midClick: true
});

//-----------------------------------------------
$("#popupForm").submit(function(event){
	event.preventDefault();
 
	// Get form values.
	var form = $(this);
	var url = form.attr("action");

	var data = {};
	form.find("[name]").each(function(i , v){
		var input = $(this),
		name = input.attr("name"),
		value = input.val();
		data[name] = value;
	});

	if(data["id"] == 0){
		return;
	}

	$.post(url, data);
	$("#popupResult").empty().append("Submitted.");
});

//-----------------------------------------------
var prev_domain = "";
$("#popupFormChkSubdomain").change(function(){
	if($(this).is(":checked")){
		var domain = $("#popupFormDomain").val();
		if(domain.indexOf("*.") != 0){
			prev_domain = domain;
		}

		domain = domain.replace(/^www\./, "");
		domain = "*." + domain;

		$("#popupFormDomain").val(domain);
		return;
	}
	$("#popupFormDomain").val(prev_domain);
});
</script>
