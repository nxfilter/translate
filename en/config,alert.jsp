<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
boolean checkParam(AlertData data){
	if (isNotEmpty(data.adminEmail) && !isValidEmail(data.adminEmail)) {
		errList.add("Invalid email.");
		return false;
	}

	if (data.period > 0 && (isEmpty(data.adminEmail) || isEmpty(data.smtpHost))) {
		errList.add("Email alert option requires admin email and SMTP host.");
		return false;
	}

	return true;
}

//-----------------------------------------------
void update(AlertDao dao){
	AlertData data = new AlertData();

	data.adminEmail = paramString("adminEmail");
	data.adminCc = paramString("adminCc");
	data.smtpHost = paramString("smtpHost");
	data.smtpPort = paramInt("smtpPort");
	data.smtpSsl = paramBoolean("smtpSsl");
	data.smtpUser = paramString("smtpUser");
	data.smtpPasswd = paramString("smtpPasswd");
	data.period = paramInt("period");

	data.alertCategoryArr = paramArray("alertCategoryArr");

	// Validate and update it.
	if(checkParam(data) && dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void test(AlertDao dao){
	AlertData data = new AlertData();

	data.adminEmail = paramString("adminEmail");
	data.smtpHost = paramString("smtpHost");
	data.smtpPort = paramInt("smtpPort");
	data.smtpSsl = paramBoolean("smtpSsl");
	data.smtpUser = paramString("smtpUser");
	data.smtpPasswd = paramString("smtpPasswd");
	data.period = paramInt("period");

	// Validate and update it.
	if(!checkParam(data) || !dao.update(data)){
		return;
	}

	try{
		dao.test();
		succList.add("Test email has been sent.");
	}
	catch(Exception e){
		errList.add(e.toString());
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
AlertDao dao = new AlertDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("test")){
	test(dao);
}

// Global.
AlertData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionTest(form){
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "test";
	form.submit();
}
</script>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">Alert</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Admin Email</td>
		<td><input type="text" name="adminEmail" value="<%= data.adminEmail%>" size="25"></td>
	</tr>

	<tr>
		<td>CC Recipients</td>
		<td>
		'CC Recipients' is only for access violation emails. Not for system failure or license related emails.<br>
		You can add multiple email addresses separated by semicolons.<br>
		<input type="text" name="adminCc" value="<%= data.adminCc%>" size="50">
		</td>
	</tr>

	<tr>
		<td>SMTP Host</td>
		<td><input type="text" name="smtpHost" value="<%= data.smtpHost%>" size="25"></td>
	</tr>

	<tr>
		<td>SMTP Port</td>
		<td><input type="text" name="smtpPort" value="<%= data.smtpPort%>" size="2"></td>
	</tr>

	<tr>
		<td>SMTP SSL</td>
		<td><input type="checkbox" class="no-border"
			name="smtpSsl" <%if(data.smtpSsl){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>SMTP User</td>
		<td><input type="text" name="smtpUser" value="<%= data.smtpUser%>" size="25"></td>
	</tr>

	<tr>
		<td>SMTP Password</td>
		<td><input type="password" name="smtpPasswd" value="<%= data.smtpPasswd%>" size="25"></td>
	</tr>

	<tr>
		<td>Alert Period</td>
		<td>
<select name="period">
<%
Map<Integer, String> periodMap = getAlertPeriodMap();
for(Map.Entry<Integer, String> entry : periodMap.entrySet()){
	Integer key = entry.getKey();
	String val = entry.getValue();

	if(key == data.period){
		printf("<option value='%s' selected>%s", key, val);
	}
	else{
		printf("<option value='%s'>%s", key, val);
	}
}
%>
</select>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="TEST" onclick="javascript:actionTest(this.form);">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">


<!--  -->
<div class="title">Alert Categories</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>
<input type="button" value="TOGGLE-ALL" onclick="javascript:checkboxToggleAll3('alertCategoryArr');">
	<br>

<%
for(int i = 0; i < data.alertCategoryList.size(); i++){
	CategoryData cd = data.alertCategoryList.get(i);

	String chkLine = "";
	if(cd.checkFlag){
		chkLine = "checked";
	}

	if(i > 0 && i % 6 == 0){
		out.println("<br>");
	}
%>
	<span class="category-item">
<input type="checkbox" class="no-border"
	name="alertCategoryArr" value="<%= cd.id%>" <%= chkLine%>><%= cd.name%>
	</span>
<%}%>

		</td>
	</tr>

	<tr height="30">
		<td align="center">
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<%@include file="include/bottom.jsp"%>
