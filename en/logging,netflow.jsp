<%@include file="include/top.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();
permission.addReport();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
NetflowDao dao = new NetflowDao();

// Set filtering option.
dao.limit = 100;
dao.page = paramInt("page", 1);
dao.stime = paramString("stime");
dao.etime = paramString("etime");

if(paramString("actionFlag").equals("userSum")){
	dao.userSumFlag = true;
}
else{
	dao.user = paramString("user");
	dao.cltIp = paramString("cltIp");
	dao.srcIp = paramString("srcIp");
	dao.dstIp = paramString("dstIp");

	dao.srcPort = paramInt("srcPort");
	dao.dstPort = paramInt("dstPort");
	dao.protocol = paramInt("protocol");

	if(isNotEmpty(dao.srcIp) && !isValidIp(dao.srcIp)){
		errList.add("Invalid source IP.");
	}

	if(isNotEmpty(dao.dstIp) && !isValidIp(dao.dstIp)){
		errList.add("Invalid destination IP.");
	}
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gTimeOption = paramString("timeOption", "2h");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionUserSum(){
	var form = document.searchForm;
	form.actionFlag.value = "userSum";
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function setUserdef(form){
	form.timeOption[0].checked = true;
}

//-----------------------------------------------
function setPeriod(form){
	var opt = radioGetValue(form.timeOption);

	var stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 2)%>';
	var etime = '<%= strftime("yyyy/MM/dd HH:mm")%>';

	if(opt == "24h"){
		stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 24)%>';
	}

	if(opt == "48h"){
		stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 48)%>';
	}

	form.stime.value = stime;
	form.etime.value = etime;
	form.stime.disabled = false;
	form.etime.disabled = false;
}

//-----------------------------------------------
function setPeriod2(form){
	if(!radioIsChecked(form.timeOption) || form.timeOption[0].checked){
		return;
	}
	setPeriod(form);
}

//-----------------------------------------------
function clearSearchForm(){
	document.searchForm.user.value = "";
	document.searchForm.cltIp.value = "";
	document.searchForm.srcIp.value = "";
	document.searchForm.dstIp.value = "";
	document.searchForm.srcPort.value = "";
	document.searchForm.dstPort.value = "";
}

//-----------------------------------------------
function searchByUser(user){
	clearSearchForm();
	document.searchForm.user.value = user;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByCltIp(cltIp){
	clearSearchForm();
	document.searchForm.cltIp.value = cltIp;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchBySrcIp(srcIp){
	clearSearchForm();
	document.searchForm.srcIp.value = srcIp;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByDstIp(dstIp){
	clearSearchForm();
	document.searchForm.dstIp.value = dstIp;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchBySrcPort(srcPort){
	clearSearchForm();
	document.searchForm.srcPort.value = srcPort;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByDstPort(dstPort){
	clearSearchForm();
	document.searchForm.dstPort.value = dstPort;
	document.searchForm.submit();
}
</script>

<!-- view -->
<form name="searchForm" action="<%= getPageName()%>" method="get">
<input type="hidden" name="actionFlag" value="">
<div class="title">NetFlow</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="100" align="right">Time :</td>
		<td colspan="7">
			<input id="stime" type="text" name="stime" value="<%= dao.getStime()%>" size="12" onchange="javascript:setUserdef(this.form)">
				~ <input id="etime" type="text" name="etime" value="<%= dao.getEtime()%>" size="12" onchange="javascript:setUserdef(this.form)">

			<div style="display: none;">
				<input type="radio" class="no-border" name="timeOption" value="userdef" onclick="javascript:setPeriod(this.form)"
					<%if(gTimeOption.equals("userdef")){out.print("checked");}%>> User defined
			</div>

			<input type="radio" class="no-border" name="timeOption" value="2h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("2h")){out.print("checked");}%>> Last 2 hours
			<input type="radio" class="no-border" name="timeOption" value="24h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("24h")){out.print("checked");}%>> Last 24 hours
			<input type="radio" class="no-border" name="timeOption" value="48h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("48h")){out.print("checked");}%>> Last 48 hours
		</td>
	</tr>

	<tr>
		<td width="100" align="right">User :</td>
		<td width="100"><input type="text" name="user" value="<%= dao.user%>" size="25"></td>

		<td width="100" align="right">Client IP :</td>
		<td width="100"><input type="text" name="cltIp" value="<%= dao.cltIp%>" size="25"></td>
		
		<td width="150" align="right">Protocol :
<select name="protocol">
<%
Map<String,String> protoMap = dao.getProtoMap();
for(Map.Entry<String,String> entry : protoMap.entrySet()){
	String kw = entry.getKey();
	String val = entry.getValue();

	if(kw.equals(dao.protocol+"")){
		printf("<option value='%s' selected>%s", kw, val);
	}
	else{
		printf("<option value='%s'>%s", kw, val);
	}
}
%>
</select>
		</td>
		<td width="150"></td>
		
		<td width="60"></td>
		<td ></td>
	</tr>

	<tr>
		<td width="100" align="right">Source IP :</td>
		<td width="100"><input type="text" name="srcIp" value="<%= dao.srcIp%>" size="25"></td>

		<td width="100" align="right">Dest IP :</td>
		<td width="100"><input type="text" name="dstIp" value="<%= dao.dstIp%>" size="25"></td>
		
		<td width="150" align="right">Source Port : <input type="text" name="srcPort" value="<%= dao.srcPort == 0 ? "" : dao.srcPort%>" size="3"></td>
		<td width="150" align="right">Dest Port : <input type="text" name="dstPort" value="<%= dao.dstPort == 0 ? "" : dao.dstPort%>" size="3"></td>
		
		<td width="60"></td>
		<td ></td>
	</tr>

	<tr height="30">
		<td></td>
		<td></td>
		<td></td>
		<td colspan="5">
<input type="button" value="SUBMIT" onclick="javascript:setPeriod2(this.form);this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="CLEAR" onclick="javascript:clearSearchForm();">
<input type="button" value="USER-SUM" onclick="javascript:actionUserSum();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%> / Page : <%= dao.page%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="10"></td></tr>
	<tr class="head">
		<td width="100">Time</td>
		<td width="150">User</td>
		<td width="150">Client IP</td>
		<td width="70" align="center">Size</td>
		<td width="30"></td>
		<td width="120">Source IP</td>
		<td width="120">Source Port</td>
		<td width="120">Dest IP</td>
		<td width="120">Dest Port</td>
		<td width="">Protocol</td>
	</tr>
	<tr class="line"><td colspan="10"></td></tr>

<%
List<NetflowData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='10' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	NetflowData data = dataList.get(i);

	// Format size.
	String fmt_size = formatFileSize(data.size);

	// For user-sum.
	if(isEmpty(data.cltIp)){
		data.cltIp = "0.0.0.0";
	}
	if(isEmpty(data.srcIp)){
		data.srcIp = "0.0.0.0";
	}
	if(isEmpty(data.dstIp)){
		data.dstIp = "0.0.0.0";
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='10'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td><a href="javascript:searchByUser('<%= data.user%>')"><%= data.user%></a></td>
		<td><a href="javascript:searchByCltIp('<%= data.cltIp%>')"><%= data.cltIp%></a></td>
		<td align="right"><%= fmt_size%></td>
		<td></td>
		<td><a href="javascript:searchBySrcIp('<%= data.srcIp%>')"><%= data.srcIp%></a></td>
		<td><a href="javascript:searchBySrcPort('<%= data.srcPort%>')"><%= data.srcPort%></a></td>
		<td><a href="javascript:searchByDstIp('<%= data.dstIp%>')"><%= data.dstIp%></a></td>
		<td><a href="javascript:searchByDstPort('<%= data.dstPort%>')"><%= data.dstPort%></a></td>
		<td><%= data.getProtocol()%></td>
	</tr>
<%}%>

	<tr class="line"><td colspan="10"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="limit" value="<%= gLimit%>">
<input type="hidden" name="timeOption" value="<%= gTimeOption%>">
<input type="hidden" name="stime" value="<%= dao.stime%>">
<input type="hidden" name="etime" value="<%= dao.etime%>">
<input type="hidden" name="user" value="<%= dao.user%>">
<input type="hidden" name="cltIp" value="<%= dao.cltIp%>">
<input type="hidden" name="srcIp" value="<%= dao.srcIp%>">
<input type="hidden" name="dstIp" value="<%= dao.dstIp%>">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>

<!-- Datetime picker -->
<script type="text/javascript">
//-----------------------------------------------
var dateToDisable = new Date();

//-----------------------------------------------
jQuery("#stime").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});

//-----------------------------------------------
jQuery("#etime").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
</script>
