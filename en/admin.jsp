<%@include file="include/lib.jsp"%>
<%
// Create data access object.
AdminLoginDao dao = new AdminLoginDao(request);

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("logout")){
	dao.logout();
}
if(actionFlag.equals("login")){
	if(dao.login(paramString("uname"), paramString("passwd"))){
		// Start page for admin.
		response.sendRedirect("dashboard.jsp");
		return;
	}
}

// Report Manager.
String reportPw = paramString("rpw");
if(isNotEmpty(reportPw)){
	AdminDao adminDao = new AdminDao();
	AdminData adminData = adminDao.selectOne();

	if(isNotEmpty(adminData.reportPw) && reportPw.equals(adminData.reportPw)){
		adminLoginDao.createReportSession();
		response.sendRedirect("report,daily.jsp");
		return;
	}
}
%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Expires" content="-1"> 
<meta http-equiv="Pragma" content="no-cache"> 
<meta http-equiv="Cache-Control" content="no-cache"> 
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<title><%= getNxName()%> v<%= getNxVersion()%></title>

<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
	width: 360px;
	padding: 8% 0 0;
	margin: auto;
}
.form {
	position: relative;
	z-index: 1;
	background: #FFFFFF;
	max-width: 360px;
	margin: 0 auto 100px;
	padding: 35px;
	text-align: center;
	box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
	font-family: "Roboto", sans-serif;
	outline: 0;
	background: #f2f2f2;
	width: 100%;
	border: 0;
	margin: 0 0 12px;
	padding: 12px;
	box-sizing: border-box;
	font-size: 14px;
}
.form button {
	font-family: "Roboto", sans-serif;
	text-transform: uppercase;
	outline: 0;
	background: #1B9BFF;
	width: 100%;
	border: 0;
	padding: 12px;
	color: #FFFFFF;
	font-size: 14px;
	-webkit-transition: all 0.3 ease;
	transition: all 0.3 ease;
	cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
	background: #0D92FF;
}
.form .message {
	margin: 15px 0 0;
	color: #b3b3b3;
	font-size: 12px;
}
.form .message a {
	color: #4CAF50;
	text-decoration: none;
}
.form .register-form {
	display: none;
}
.container {
	position: relative;
	z-index: 1;
	max-width: 300px;
	margin: 0 auto;
}
.container:before, .container:after {
	content: "";
	display: block;
	clear: both;
}
.container .info {
	margin: 50px auto;
	text-align: center;
}
.container .info h1 {
	margin: 0 0 15px;
	padding: 0;
	font-size: 36px;
	font-weight: 300;
	color: #1a1a1a;
}
.container .info span {
	color: #4d4d4d;
	font-size: 12px;
}
.container .info span a {
	color: #000000;
	text-decoration: none;
}
.container .info span .fa {
	color: #EF3B3A;
}

.myinfo{
	font-size: 14px;
	margin: 50px auto;
}

body {
	background: #A2A2A2; /* fallback for old browsers */
	font-family: "Roboto", sans-serif;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;      
}
</style>
<script type="text/javascript">
$('.message a').click(function(){
	$('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});	
</script>
</head>

<body>

<div class="login-page">
	<div class="form">
		<form method="post" action="<%= getPageName()%>" class="login-form">
			<input type="hidden" name="actionFlag" value="login">
			<input type="text" name="uname" placeholder="Username"/>
			<input type="password" name="passwd" placeholder="Password"/>
			<button>login</button>

			<%if(dao.isFirstLogin()){%>
				<p class="message">Initial username and password : admin/admin</p>
			<%}%>

		</form>
	</div>
</div>

</body>
</html>
