<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(PolicyDao dao){
	PolicyData data = new PolicyData();
	data.name = paramString("name");
	data.description = paramString("description");
	data.copyId = paramInt("copyId");

	// Param validation.
	if(!ParamTest.isValidNameLen(data.name)){
		errList.add(ParamTest.ERR_NAME_LEN);
		return;
	}
	
	if(!ParamTest.isValidUsernameChar(data.name)){
		errList.add(ParamTest.ERR_NAME_CHAR);
		return;
	}

	if (ParamTest.isDupPolicy(data.name)) {
		errList.add("Policy already exists.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(PolicyDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
PolicyDao dao = new PolicyDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

// Global.
int gCount = dao.selectCount();

// Get policy list.
List<PolicyData> gPolicyList = dao.selectList();

if (gCount > 1 && new CategorySystemDao().getBlacklistType() == 8) {
	warnList.add("Globlist doesn't support multiple policies. Default policy will be applied to all of your users.");
}
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, name){
	if(!confirm("Deleting policy: " + name)){
		return;
	}
	
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goEdit(id){
	var form = document.goForm;
	form.action = "policy,policy_edit.jsp";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">Policy</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Name</td>
		<td>
			<input type="text" name="name" size="25">
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50"></td>
	</tr>

	<tr>
		<td>Template Policy</td>
		<td>
<select name="copyId">
	<option value="0">Select a policy
<%
for(PolicyData data : gPolicyList){
	printf("<option value='%s'>%s", data.id, data.name);
}
%>
</select>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="5"></td></tr>
	<tr class="head">
		<td width="200">Name</td>
		<td width="100" align="right">Priority Points</td>
		<td width="100"></td>
		<td width="">Description</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="5"></td></tr>

<%
for(int i = 0; i < gPolicyList.size(); i++){
	PolicyData data = gPolicyList.get(i);

	String name = data.name;
	if(data.systemFlag){
		name = "*" + name;
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='5'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= name%></td>
		<td align="right"><%= data.points%></td>
		<td></td>
		<td><%= data.description%></td>
		<td align="right">
		<input type="button" value="EDIT" onclick="javascript:goEdit(<%= data.id%>)">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.name%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="5"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
