<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(ZoneFileDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	ZoneFileData data = new ZoneFileData();
	data.id = paramInt("id");
	data.zone = paramString("zone");
	data.bypassAuth = paramBoolean("bypassAuth");
	data.bypassFilter = paramBoolean("bypassFilter");
	data.bypassLog = paramBoolean("bypassLog");
	data.description = paramString("description");

	try{
		dao.testZone(data);
	}
	catch(Exception e){
		errList.add(e.toString());
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ZoneFileDao dao = new ZoneFileDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
ZoneFileData data = dao.selectOne(paramInt("id"));
%>
<%@include file="include/action_info.jsp"%>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">
<input type="hidden" name="id" value="<%= data.id%>">

<!--  -->
<div class="title">Zone File</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Domain</td>
		<td>
			<%= data.domain%>
		</td>
	</tr>

	<tr>
		<td>Bypass Authentication</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassAuth" <%if(data.bypassAuth){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Bypass Filtering</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassFilter" <%if(data.bypassFilter){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Bypass Logging</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassLog" <%if(data.bypassLog){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50" value="<%= data.description%>"></td>
	</tr>

	<tr>
		<td>Zone</td>
		<td>
			<textarea name="zone" cols="80" rows="30"><%= data.zone%></textarea>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
