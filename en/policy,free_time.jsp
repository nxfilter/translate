<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(FreeTimeDao dao){
	String stimeHh = paramString("stimeHh");
	String stimeMm = paramString("stimeMm");
	String etimeHh = paramString("etimeHh");
	String etimeMm = paramString("etimeMm");

	FreeTimeData data = new FreeTimeData();
	data.stime = stimeHh + stimeMm;
	data.etime = etimeHh + etimeMm;
	data.wdayArr = paramArray("wdayArr");
	data.description = paramString("description");

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(FreeTimeDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
FreeTimeDao dao = new FreeTimeDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

// Global.
int gCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id){
	if(!confirm("Deleting free-time?")){
		return;
	}

	var form = document.goForm;
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">Free Time</div>

<%
List<String> hhList = getHhList();
List<String> mmList = getMmList();
%>
<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>Time</td>
		<td>
<select name="stimeHh">
<%
for(String hh : hhList){
	printf("<option value='%s'>%s</option>\n", hh, hh);
}
%>
</select>

<select name="stimeMm">
<%
for(String mm : mmList){
	printf("<option value='%s'>%s</option>\n", mm, mm);
}
%>
</select>
 ~
<select name="etimeHh">
<%
for(String hh : hhList){
	printf("<option value='%s'>%s</option>\n", hh, hh);
}
%>
</select>

<select name="etimeMm">
<%
for(String mm : mmList){
	printf("<option value='%s'>%s</option>\n", mm, mm);
}
%>
</select>

		</td>
	</tr>

	<tr>
		<td width="200">Day of Week</td>
		<td>
			<input type="button" value="TOGGLE-ALL" onclick="javascript:checkboxToggleAll3('wdayArr');"><br>
			<input type="checkbox" class="no-border" name="wdayArr" value="2">Mon
			<input type="checkbox" class="no-border" name="wdayArr" value="3">Tue
			<input type="checkbox" class="no-border" name="wdayArr" value="4">Wed
			<input type="checkbox" class="no-border" name="wdayArr" value="5">Thu
			<input type="checkbox" class="no-border" name="wdayArr" value="6">Fri
			<input type="checkbox" class="no-border" name="wdayArr" value="7">Sat
			<input type="checkbox" class="no-border" name="wdayArr" value="1">Sun
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50"></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="4"></td></tr>
	<tr class="head">
		<td width="200">Time</td>
		<td width="300">Day of Week</td>
		<td width="">Description</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="4"></td></tr>

<%
List<FreeTimeData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='4' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	FreeTimeData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='4'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.stime%> ~ <%= data.etime%></td>
		<td><%= data.getWdayLine()%></td>
		<td><%= data.description%></td>
		<td align="right">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>)">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="4"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form name="goForm" method="get">
<input type="hidden" name="mode" value="">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
