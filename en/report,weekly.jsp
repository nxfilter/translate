<%@include file="include/top.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();
permission.addReport();

//Check permission.
if(!checkPermission()){
	return;
}

// If there's a user it becomes user specific report.
String stime = paramString("stime");
String user = paramString("user");

// Create data access object.
D7ReportDao dao = new D7ReportDao(stime, user);
ReportStatsData stats = dao.getStats();
ReportChartData requestTrend = dao.getRequestTrend();
ReportChartData domainTop = dao.getDomainTop(5);
ReportChartData categoryTop = dao.getCategoryTop(5);
ReportChartData userTop = dao.getUserTop(5);
ReportChartData cltIpTop = dao.getCltIpTop(5);

// Global.
String gTimeOption = paramString("timeOption", "last7days");
String gUser = paramString("user");
%>

<script type='text/javascript'>
//-----------------------------------------------
function setUserdef(form){
	form.timeOption[0].checked = true;
}

//-----------------------------------------------
function setPeriod(form){
	var opt = radioGetValue(form.timeOption);

	var stime = '<%= strftimeAdd("yyyy/MM/dd", -86400 * 7)%>';

	if(opt == '1week'){
		stime = '<%= strftimeAdd("yyyy/MM/dd", -86400 * 7 * 2)%>';
	}

	if(opt == '2weeks'){
		stime = '<%= strftimeAdd("yyyy/MM/dd", -86400 * 7 * 3)%>';
	}

	if(opt == '3weeks'){
		stime = '<%= strftimeAdd("yyyy/MM/dd", -86400 * 7 * 4)%>';
	}

	form.stime.value = stime;
	form.stime.disabled = false;
}

//-----------------------------------------------
function setPeriod2(form){
	if(!radioIsChecked(form.timeOption) || form.timeOption[0].checked){
		return;
	}
	setPeriod(form);
}
</script>

<!-- Google chart -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});

// Draw request trend chart.
google.setOnLoadCallback(drawRequestTrend);
function drawRequestTrend() {
	var data = google.visualization.arrayToDataTable([
		["Time", "Request"]
<%
List<String[]> arrList = requestTrend.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Request trend",
		legend: {position: "none"}
	};

	var chart = new google.visualization.LineChart(document.getElementById("chartRequestTrend"));
	chart.draw(data, options);
}

// Draw block trend chart.
google.setOnLoadCallback(drawBlockTrend);
function drawBlockTrend() {
	var data = google.visualization.arrayToDataTable([
		["Time", "Block"]
<%
arrList = requestTrend.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Block trend",
		legend: {position: "none"}
	};

	var chart = new google.visualization.LineChart(document.getElementById("chartBlockTrend"));
	chart.draw(data, options);
}

// Draw domain top chart.
google.setOnLoadCallback(drawDomainTop);
function drawDomainTop() {
	var data = google.visualization.arrayToDataTable([
		["Domain", "Request"]
<%
arrList = domainTop.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 domains by request",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartDomainTop"));
	chart.draw(data, options);
}

// Draw domain block chart.
google.setOnLoadCallback(drawDomainBlock);
function drawDomainBlock() {
	var data = google.visualization.arrayToDataTable([
		["Domain", "Block"]
<%
arrList = domainTop.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 domains by block",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartDomainBlock"));
	chart.draw(data, options);
}

// Draw category top chart.
google.setOnLoadCallback(drawCategoryTop);
function drawCategoryTop() {
	var data = google.visualization.arrayToDataTable([
		["Category", "Request"]
<%
arrList = categoryTop.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 categories by request",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartCategoryTop"));
	chart.draw(data, options);
}

// Draw category block chart.
google.setOnLoadCallback(drawCategoryBlock);
function drawCategoryBlock() {
	var data = google.visualization.arrayToDataTable([
		["Category", "Block"]
<%
arrList = categoryTop.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 categories by block",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartCategoryBlock"));
	chart.draw(data, options);
}

// Draw user top chart.
google.setOnLoadCallback(drawUserTop);
function drawUserTop() {
	var data = google.visualization.arrayToDataTable([
		["User", "Request"]
<%
arrList = userTop.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 users by request",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartUserTop"));
	chart.draw(data, options);
}

// Draw user block chart.
google.setOnLoadCallback(drawUserBlock);
function drawUserBlock() {
	var data = google.visualization.arrayToDataTable([
		["User", "Block"]
<%
arrList = userTop.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 users by block",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartUserBlock"));
	chart.draw(data, options);
}

// Draw cltIp top chart.
google.setOnLoadCallback(drawCltIpTop);
function drawCltIpTop() {
	var data = google.visualization.arrayToDataTable([
		["IP", "Request"]
<%
arrList = cltIpTop.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 client-ip by request",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartCltIpTop"));
	chart.draw(data, options);
}

// Draw cltIp block chart.
google.setOnLoadCallback(drawCltIpBlock);
function drawCltIpBlock() {
	var data = google.visualization.arrayToDataTable([
		["IP", "Block"]
<%
arrList = cltIpTop.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 client-ip by block",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartCltIpBlock"));
	chart.draw(data, options);
}
</script>
<!-- /Google chart -->

<!-- view -->
<form action="<%= getPageName()%>" method="get">
<div class="title">Weekly Report</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="100" align="right">
	Report from :
		</td>
		<td>
			<input id="stime" type="text" value="<%= dao.getStime()%>" name="stime" size="8" onchange="javascript:setUserdef(this.form)">

			<div style="display: none;">
				<input type="radio" class="no-border" name="timeOption" value="userdef" onclick="javascript:setPeriod(this.form)"
					<%if(gTimeOption.equals("userdef")){out.print("checked");}%>> User defined
			</div>

			<input type="radio" class="no-border" name="timeOption" value="last7days" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("last7days")){out.print("checked");}%>> Last 7 days
			<input type="radio" class="no-border" name="timeOption" value="1week" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("1week")){out.print("checked");}%>> 1 week ago
			<input type="radio" class="no-border" name="timeOption" value="2weeks" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("2weeks")){out.print("checked");}%>> 2 weeks ago
			<input type="radio" class="no-border" name="timeOption" value="3weeks" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("3weeks")){out.print("checked");}%>> 3 weeks ago
		</td>
	</tr>

	<tr>
		<td width="100" align="right">
	User :
		</td>
		<td>
			<input type="text" value="<%= gUser%>" name="user" size="25">
			<select onchange="javascript:this.form.user.value=this.value">
			<option value=""> Select a user
<%
List<String> userList = dao.getLogUserList();
for(String uname : userList){
	printf("<option value='%s'>%s", uname, uname);
}
%>
			</select>
		</td>

	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:setPeriod2(this.form);this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<div class="title">
<%= dao.getStime()%> ~ <%= dao.getEtime()%>
<%
if(!isEmpty(gUser)){
	out.print(", " + gUser);
}
%>
</div>
<div class="stats">
request-sum = <%= stats.reqSum%>,
request-cnt = <%= stats.reqCnt%>,
block-sum = <%= stats.blockSum%>,
block-cnt = <%= stats.blockCnt%>,
domain = <%= stats.domainCnt%>,
user = <%= stats.userCnt%>,
client-ip = <%= stats.cltIpCnt%>
</div>

<table width="98%" cellpadding="0" cellspacing="0">
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td width="650">
    <div id="chartRequestTrend" style="width: 650px; height: 300px;"></div>
	</td>
	<td width="650">
    <div id="chartBlockTrend" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td>
    <div id="chartDomainTop" style="width: 650px; height: 300px;"></div>
	</td>
	<td>
    <div id="chartDomainBlock" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td>
    <div id="chartCategoryTop" style="width: 650px; height: 300px;"></div>
	</td>
	<td>
    <div id="chartCategoryBlock" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>

<%if(isEmpty(gUser)){%>
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td>
    <div id="chartUserTop" style="width: 650px; height: 300px;"></div>
	</td>
	<td>
    <div id="chartUserBlock" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td>
    <div id="chartCltIpTop" style="width: 650px; height: 300px;"></div>
	</td>
	<td>
    <div id="chartCltIpBlock" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
<%}%>

</table>

<%@include file="include/bottom.jsp"%>

<!-- Datetime picker -->
<script type="text/javascript">
//-----------------------------------------------
var dateToDisable = new Date();
dateToDisable.setDate(dateToDisable.getDate() - 6);

//-----------------------------------------------
jQuery("#stime").datetimepicker({
	timepicker: false,
	format: "Y/m/d",
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
</script>
