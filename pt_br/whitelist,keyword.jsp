<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(WhitelistKeywordDao dao){
	WhitelistData data = new WhitelistData();
	data.keyword = paramString("keyword");
	data.description = paramString("description");

	data.bypassAuth = paramBoolean("bypassAuth");
	data.bypassFilter = paramBoolean("bypassFilter");
	data.bypassLog = paramBoolean("bypassLog");
	data.adminBlock = paramBoolean("adminBlock");
	data.dropPacket = paramBoolean("dropPacket");

	// Param validation.
	if(isEmpty(data.keyword) || data.keyword.matches(".*\\s+.*")){
		errList.add("Invalid keyword.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(WhitelistKeywordDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
WhitelistKeywordDao dao = new WhitelistKeywordDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

// Global.
int gCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, keyword){
	if(!confirm("Deleting keyword : " + keyword)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goEdit(id){
	var form = document.goForm;
	form.action = "whitelist,keyword_edit.jsp";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">Whitelist by Keyword</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Keyword</td>
		<td>
<div style="line-height:20px;">
ex) porn, porn*site
</div>
			<input type="text" name="keyword" size="25">
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50"></td>
	</tr>

	<tr>
		<td>Bypass Authentication</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassAuth">
		</td>
	</tr>

	<tr>
		<td>Bypass Filtering</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassFilter">
		</td>
	</tr>

	<tr>
		<td>Bypass Logging</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassLog">
		</td>
	</tr>

	<tr>
		<td>Admin Block</td>
		<td>
			<input type="checkbox" class="no-border" name="adminBlock">
		</td>
	</tr>

	<tr>
		<td>Drop Packet</td>
		<td>
			<input type="checkbox" class="no-border" name="dropPacket">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="5"></td></tr>
	<tr class="head">
		<td width="200">Keyword</td>
		<td width="400">Flags</td>
		<td width="400">Applied Policy</td>
		<td width="">Description</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="5"></td></tr>

<%
List<WhitelistData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='5' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	WhitelistData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='5'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.keyword%></td>
		<td><%= data.getFlagLine()%></td>
		<td><%= data.getAppliedPolicyLine()%></td>
		<td><%= data.description%></td>
		<td align="right">
			<input type="button" value="EDIT" onclick="javascript:goEdit(<%= data.id%>)">
			<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.keyword%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="5"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
