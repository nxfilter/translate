<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(AdapDao dao){
	LdapData data = new LdapData();
	data.id = paramInt("id");
	data.host = paramString("host");
	data.admin = paramString("admin");
	data.passwd = paramString("passwd");
	data.basedn = paramString("basedn");
	data.domain = paramString("domain");
	data.followReferral = paramBoolean("followReferral");
	data.period = paramInt("period");
	data.excludeKeyword = paramString("excludeKeyword");

    data.dnsIp = paramString("dnsIp");
    data.dnsTimeout = paramInt("dnsTimeout");

	data.useSsl = paramBoolean("useSsl");
    data.port = paramInt("port");
	data.sslCertificateCn = paramString("sslCertificateCn");

	// Param validation.
	if (!isValidIp(data.host)) {
		errList.add("Invalid host IP.");
		return;
	}

	if (isEmpty(data.admin)) {
		errList.add("Admin missing.");
		return;
	}

	if (isEmpty(data.basedn)) {
		errList.add("Base DN missing.");
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
AdapDao dao = new AdapDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
LdapData data = dao.selectOne(paramInt("id"));
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionUpdate(form){
	if(form.dnsIp.value.indexOf(form.host.value) == -1 && !confirm("MS DNS server IP is different from host IP address?")){
		return;
	}

	form.submit();
}

//-----------------------------------------------
function setDefaultPort(form){
	var port = form.port.value;

	if(form.useSsl.checked && port == 389){
		form.port.value = 636;
	}

	if(!form.useSsl.checked && port == 636){
		form.port.value = 389;
	}

	if(!form.useSsl.checked){
		form.sslCertificateCn.value = ""
	}
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">
<input type="hidden" name="id" value="<%= data.id%>">

<div class="title">Active Directory</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="200">Host</td>
		<td><input type="text" name="host" size="25" value="<%= data.host%>"></td>
	</tr>

	<tr>
		<td>Port</td>
		<td><input type="text" name="port" size="2" maxlength="5" value="<%= data.port%>"></td>
	</tr>

	<tr>
		<td>Use SSL</td>
		<td>
		<input type="checkbox" name="useSsl" <%if(data.useSsl){out.print("checked");}%>
			onclick="javascript:setDefaultPort(this.form);">
		</td>
	</tr>

	<tr>
		<td>SSL Certificate CN</td>
		<td>
		When you use your own certificate or when you want to use a hostname other than an IP address for LDAPS protocol.<br>
		<input type="text" name="sslCertificateCn" size="40" value="<%= data.sslCertificateCn%>">
		</td>
	</tr>

	<tr>
		<td>Admin</td>
		<td><input type="text" name="admin" size="25" value="<%= data.admin%>"></td>
	</tr>

	<tr>
		<td>Password</td>
		<td><input type="password" name="passwd" size="25" value="<%= data.passwd%>"></td>
	</tr>

	<tr>
		<td>Base DN</td>
		<td><input type="text" name="basedn" size="40" value="<%= data.basedn%>"></td>
	</tr>

	<tr>
		<td>Domain</td>
		<td><input type="text" name="domain" size="40" value="<%= data.domain%>"></td>
	</tr>

	<tr>
		<td>Follow Referral</td>
		<td><input type="checkbox" class="no-border"
			name="followReferral" <%if(data.followReferral){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Auto-sync</td>
		<td>
<select name="period">
<%
Map<Integer, String> periodMap = getLdapPeriodMap();
for(Map.Entry<Integer, String> entry : periodMap.entrySet()){
	Integer key = entry.getKey();
	String val = entry.getValue();

	if(key == data.period){
		printf("<option value='%s' selected>%s", key, val);
	}
	else{
		printf("<option value='%s'>%s", key, val);
	}
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Exclude Keyword</td>
		<td>
			You can exclude some of users or groups based on keyword matching when you don't want to import<br>
			certain users or groups. You can add keywords separated by spaces. For a keyword having space use double quotes,<br>
			for exact matching use square brackets.<br>
			&nbsp;&nbsp;ex) support devel [DHCP Users] "main Co" john<br>
			<textarea name="excludeKeyword" cols="80" rows="6"><%= data.excludeKeyword%></textarea>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:actionUpdate(this.form);">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

<!-- MS DNS -->
<div class="title">MS DNS</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="200">DNS IP</td>
		<td>
		You can add multiple IP addresses separated by commas for redundancy or load banlacing.<br>
		<input type="text" name="dnsIp" size="25" value="<%= data.dnsIp%>">
		</td>
	</tr>

	<tr>
		<td>DNS Query Timeout</td>
		<td><input type="text" name="dnsTimeout" size="2" maxlength="2" value="<%= data.dnsTimeout%>"> seconds, 1 ~ 20</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:actionUpdate(this.form);">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<!-- view -->

<%@include file="include/bottom.jsp"%>
