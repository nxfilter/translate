<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(ClassifierSetupDao dao){
	ClassifierSetupData data = new ClassifierSetupData();

	data.dnsTestTimeout = paramInt("dnsTestTimeout");
	data.httpConnTimeout = paramInt("httpConnTimeout");
	data.httpReadTimeout = paramInt("httpReadTimeout");
	data.classifiedRetentionDays = paramInt("classifiedRetentionDays");
	data.keepHtmlText = paramBoolean("keepHtmlText");
	data.disableDomainPatternDic = paramBoolean("disableDomainPatternDic");
	data.disableCloudClassifier = paramBoolean("disableCloudClassifier");
	data.disableClassification = paramBoolean("disableClassification");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ClassifierSetupDao dao = new ClassifierSetupDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// If it's about importation.
int importCount = paramInt("importCount");
if(importCount > 0){
	if(actionFlag.equals("ruleset")){
		succList.add(importCount + " classification rules imported.");
	}
	else{
		succList.add(importCount + " domains imported.");
	}
}

// Global.
ClassifierSetupData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<!--  -->
<div class="title">Classifier Setup</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="210">DNS Test Timeout</td>
		<td><input type="text" name="dnsTestTimeout" value="<%= data.dnsTestTimeout%>" size="2" maxlength="2"></td>
	</tr>

	<tr>
		<td>HTTP Connection Timeout</td>
		<td><input type="text" name="httpConnTimeout" value="<%= data.httpConnTimeout%>" size="2" maxlength="2"></td>
	</tr>

	<tr>
		<td>HTTP Read Timeout</td>
		<td><input type="text" name="httpReadTimeout" value="<%= data.httpReadTimeout%>" size="2"></td>
	</tr>

	<tr>
		<td>Classified Log Retention Days</td>
		<td><input type="text" name="classifiedRetentionDays" value="<%= data.classifiedRetentionDays%>" size="2"></td>
	</tr>

	<tr>
		<td>Keep HTML Text</td>
		<td><input type="checkbox" class="no-border"
			name="keepHtmlText" <%if(data.keepHtmlText){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Disable Domain Pattern Analyzer</td>
		<td><input type="checkbox" class="no-border"
			name="disableDomainPatternDic" <%if(data.disableDomainPatternDic){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Disable Cloud Classifier</td>
		<td><input type="checkbox" class="no-border"
			name="disableCloudClassifier" <%if(data.disableCloudClassifier){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Disable Classification</td>
		<td><input type="checkbox" class="no-border"
			name="disableClassification" <%if(data.disableClassification){out.print("checked");}%>></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>

<%@include file="include/bottom.jsp"%>
