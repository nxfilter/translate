<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(EdapDao dao){
	LdapData data = new LdapData();
	data.id = paramInt("id");
	data.host = paramString("host");
	data.admin = paramString("admin");
	data.passwd = paramString("passwd");
	data.basedn = paramString("basedn");
	data.ldapType = LdapData.TYPE_ED;
	data.period = paramInt("period");
	data.excludeKeyword = paramString("excludeKeyword");

	data.useSsl = paramBoolean("useSsl");
    data.port = paramInt("port");

	// Param validation.
	if (!isValidIp(data.host)) {
		errList.add("Invalid host IP.");
		return;
	}

	if (isEmpty(data.admin)) {
		errList.add("Admin missing.");
		return;
	}

	if (isEmpty(data.basedn)) {
		errList.add("Base DN missing.");
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%@include file="include/action_info.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
EdapDao dao = new EdapDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
LdapData data = dao.selectOne(paramInt("id"));
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function setDefaultPort(form){
	var port = form.port.value;

	if(form.useSsl.checked && port == 389){
		form.port.value = 636;
	}

	if(!form.useSsl.checked && port == 636){
		form.port.value = 389;
	}
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">
<input type="hidden" name="id" value="<%= data.id%>">

<div class="title">OpenLDAP</div>

<hr size="1">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="200">Host</td>
		<td><input type="text" name="host" size="25" value="<%= data.host%>"></td>
	</tr>

	<tr>
		<td>Port</td>
		<td><input type="text" name="port" size="2" maxlength="5" value="<%= data.port%>"></td>
	</tr>

	<tr>
		<td>Use SSL</td>
		<td>
		<input type="checkbox" name="useSsl" <%if(data.useSsl){out.print("checked");}%>
			onclick="javascript:setDefaultPort(this.form);">
		</td>
	</tr>

	<tr>
		<td>Admin</td>
		<td><input type="text" name="admin" size="25" value="<%= data.admin%>"></td>
	</tr>

	<tr>
		<td>Password</td>
		<td><input type="password" name="passwd" size="25" value="<%= data.passwd%>"></td>
	</tr>

	<tr>
		<td>Base DN</td>
		<td><input type="text" name="basedn" size="40" value="<%= data.basedn%>"></td>
	</tr>

	<tr>
		<td>Auto-sync</td>
		<td>
<select name="period">
<%
Map<Integer, String> periodMap = getLdapPeriodMap();
for(Map.Entry<Integer, String> entry : periodMap.entrySet()){
	Integer key = entry.getKey();
	String val = entry.getValue();

	if(key == data.period){
		printf("<option value='%s' selected>%s", key, val);
	}
	else{
		printf("<option value='%s'>%s", key, val);
	}
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Exclude Keyword</td>
		<td>
			You can exclude some of users or groups based on keyword matching when you don't want to import<br>
			certain users or groups. You can add keywords separated by spaces. For a keyword having space use double quotes,<br>
			for exact matching use square brackets.<br>
			&nbsp;&nbsp;ex) support devel [DHCP Users] "main Co" john<br>
			<textarea name="excludeKeyword" cols="80" rows="6"><%= data.excludeKeyword%></textarea>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>

</table>
<hr size="1">

</form>

<!-- view -->

<%@include file="include/bottom.jsp"%>
