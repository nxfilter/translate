<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void quotaReset(UserTestDao dao){
	if(dao.quotaReset(paramString("name"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void bwdtReset(UserTestDao dao){
	if(dao.bwdtReset(paramString("name"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
UserTestDao dao = new UserTestDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("quotaReset")){
	quotaReset(dao);
}
if(actionFlag.equals("bwdtReset")){
	bwdtReset(dao);
}

// Global.
UserTestData data = dao.test(paramString("name"));
%>

<script type="text/javascript">
//-----------------------------------------------
function actionQuotaResetUser(form){
	form.actionFlag.value = "quotaReset";
	form.submit();
}

//-----------------------------------------------
function actionBandwidthResetUser(form){
	form.actionFlag.value = "bwdtReset";
	form.submit();
}
</script>
<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="quotaReset">
<input type="hidden" name="name" value="<%= data.name%>">

<!--  -->
<div class="title">User</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="100">Name : </td>
		<td>
			<%= data.name%>
		</td>
	</tr>
	<tr>
		<td>Group : </td>
		<td>
			<%= data.groupName%>
		</td>
	</tr>
	<tr>
		<td>User Policy : </td>
		<td>
			<%= data.policyName%>
		</td>
	</tr>
	<tr>
		<td>Applied Policy : </td>
		<td>
			<%= data.appliedPolicyName%>
		</td>
	</tr>
	<tr>
		<td>Free-time : </td>
		<td>
			<%= data.isFreeTime ? "Yes" : "No"%>
		</td>
	</tr>
	<tr>
		<td>Quota : </td>
		<td>
			<%= data.quotaConsumed%> / <%= data.quotaLimit%> minutes
			<input type="button" value="RESET" onclick="javascript:actionQuotaResetUser(this.form)">
		</td>
	</tr>
	<tr>
		<td>Bandwidth : </td>
		<td>
			<%= data.bandwidthConsumed%> / <%= data.bandwidthLimit%> MB
			<input type="button" value="RESET" onclick="javascript:actionBandwidthResetUser(this.form)">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!-- /view -->

<%@include file="include/bottom.jsp"%>
