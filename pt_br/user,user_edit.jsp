<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
boolean checkParam(UserData data){
	// Check password only if there's a password updated.
	if (!isEmpty(data.passwd) && !isSha1Hex(data.passwd)) {
		if (!ParamTest.isValidPasswdLen(data.passwd)) {
			errList.add(ParamTest.ERR_PASSWD_LEN);
			return false;
		}

		if (!ParamTest.isValidPasswdChar(data.passwd)) {
			errList.add(ParamTest.ERR_PASSWD_CHAR);
			return false;
		}
	}

	// token.
	if (!isEmpty(data.token) && !ParamTest.isValidToken(data.token)) {
		errList.add(ParamTest.ERR_TOKEN_INVALID);
		return false;
	}

	return true;
}

//-----------------------------------------------
void update(UserDao dao){
	UserData data = new UserData();
	data.id = paramInt("id");
	data.passwd = paramString("passwd");
	data.policyId = paramInt("policyId");
	data.ftPolicyId = paramInt("ftPolicyId");
	data.expDate = paramString("expDate");
	data.token = paramString("token");
	data.description = paramString("description");

	// Only for manually created users.
	data.grpId = paramInt("grpId");

	// Validate and update it.
	if(checkParam(data) && dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void newToken(UserDao dao){
	UserData data = new UserData();
	data.id = paramInt("id");
	data.token = paramString("token");

	if(dao.newToken(data.id)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void addIp(UserDao dao){
	UserIpData data = new UserIpData();
	data.userId = paramInt("id");
	data.startIp = paramString("startIp");
	data.endIp = paramString("endIp");

	// Param validation.
	if (!isValidIp(data.startIp)) {
		errList.add("Invalid start IP.");
		return;
	}

	if (isNotEmpty(data.endIp) && !isValidIp(data.endIp)) {
		errList.add("Invalid end IP.");
		return;
	}

	if(dao.addIp(data)){
		succList.add("Data updated.");

		if(!new ConfigDao().selectOne().enableLogin){
			warnList.add("Authentication needs to be enabled.");
		}
	}
}

//-----------------------------------------------
void deleteIp(UserDao dao){
	if(dao.deleteIp(paramInt("ipId"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
UserDao dao = new UserDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("newToken")){
	newToken(dao);
}
if(actionFlag.equals("addIp")){
	addIp(dao);
}
if(actionFlag.equals("deleteIp")){
	deleteIp(dao);
}

// Global.
UserData data = dao.selectOne(paramInt("id"));

// Get policy list.
List<PolicyData> gPolicyList = new PolicyDao().selectList();
List<GroupData> gGroupList = new GroupDao().selectListUserCreatedOnly();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionNewToken(form){
	form.actionFlag.value = "newToken";
	form.submit();
}

//-----------------------------------------------
function actionUpdate(form){
	form.actionFlag.value = "update";
	form.submit();
}

//-----------------------------------------------
function actionAddIp(form){
	form.actionFlag.value = "addIp";
	form.submit();
}

//-----------------------------------------------
function actionDeleteIp(ipId){
	form = document.all("userEditForm");
	form.actionFlag.value = "deleteIp";
	form.ipId.value = ipId;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post" name="userEditForm">
<input type="hidden" name="actionFlag" value="edit">
<input type="hidden" name="id" value="<%= data.id%>">
<input type="hidden" name="ipId" value="">

<!--  -->
<div class="title">User</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Name</td>
		<td>
			<%= data.name%>
		</td>
	</tr>

	<tr>
		<td>Password</td>
		<td><input type="password" name="passwd" size="25" maxlength="12" value="<%= data.passwd%>"></td>
	</tr>

	<tr>
		<td>Work-time Policy</td>
		<td>
<select name="policyId">
<%
for(PolicyData pd : gPolicyList){
	if(pd.id == data.policyId){
		printf("<option value='%s' selected>%s\n", pd.id, pd.name);
	}
	else{
		printf("<option value='%s'>%s\n", pd.id, pd.name);
	}
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Free-time Policy</td>
		<td>
<select name="ftPolicyId">
	<option value="0">Same as work-time policy
<%
for(PolicyData pd : gPolicyList){
	if(pd.id == data.ftPolicyId){
		printf("<option value='%s' selected>%s\n", pd.id, pd.name);
	}
	else{
		printf("<option value='%s'>%s\n", pd.id, pd.name);
	}
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Expiration Date</td>
		<td>
		<input id="expDate" type="text" name="expDate" value="<%= data.getExpDate()%>" size="14">
		<input type="button" value="CLEAR" onclick="javascript:this.form.expDate.value='';">
		</td>
	</tr>

	<tr>
		<td>Login Token</td>
		<td>
		<input type="text" name="token" size="8" maxlength="8" value="<%= data.token%>">
		<input type="button" value="NEW-TOKEN" onclick="javascript:actionNewToken(this.form)">
		</td>
	</tr>

	<tr>
		<td>Group, Member of</td>

		<td>
		<%
		if(data.ldapId > 0){
			out.println(data.getGroupLine());
		}
		else{
			out.println("<select name='grpId'>");
			out.println("<option value='0'>anon-grp");

			for(GroupData grp : gGroupList){
				if(grp.name.startsWith(data.getGroupLine())){
					printf("<option value='%s' selected>%s\n", grp.id, grp.name);
				}
				else{
					printf("<option value='%s'>%s\n", grp.id, grp.name);
				}
			}

			out.println("</select>");
		}
		%>
		</td>

	</tr>

	<tr>
		<td>Description</td>
		<td><input type="text" name="description" size="50" value="<%= data.description%>"></td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:actionUpdate(this.form);">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

<!--  -->
<div class="title">Add IP</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height:20px;">
You can associate an IP address or an IP address range to a user. If it is a single IP association,<br>
add 'Start IP' only.
</div>
		</td>
	</tr>

	<tr>
		<td colspan="2">
		</td>
	</tr>

	<tr>
		<td width="100">Start IP</td>
		<td>
		<input type="text" name="startIp" size="25">
		<input type="button" value="ADD" onclick="javascript:actionAddIp(this.form)">
		</td>
	</tr>
	<tr>
		<td>End IP</td>
		<td>
		<input type="text" name="endIp" size="25">
		</td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="2">
<%
for(int i = 0; i < data.ipList.size(); i++){
	UserIpData uip = data.ipList.get(i);

	if(i > 0 && i % 4 == 0){
		out.print("<br>");
	}

	printf("<span class='domain-item'><a href='javascript:actionDeleteIp(%s)'>[x]</a> %s</span>", uip.id, uip.asString());
}
%>
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>

<!-- Datetime picker -->
<script type="text/javascript">
//-----------------------------------------------
var dateToDisable = new Date();

//-----------------------------------------------
jQuery("#expDate").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
</script>
