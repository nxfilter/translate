<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(ZoneTransferDao dao){
	ZoneTransferData data = new ZoneTransferData();
	data.id = paramInt("id");
	data.domain = paramString("domain");
	data.ip = paramString("ip");

	if (isEmpty(data.domain)) {
		errList.add("Domain missing.");
		return;
	}

	if (!isValidIp(data.ip)) {
		errList.add("Invalid DNS server IP.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(ZoneTransferDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void test(ZoneTransferDao dao){
	try{
		dao.test(paramInt("id"));
		succList.add("It worked.");
	}
	catch(Exception e){
		errList.add(e.toString());
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ZoneTransferDao dao = new ZoneTransferDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("test")){
	test(dao);
}

// Global.
int gCount = dao.selectCount();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, domain){
	if(!confirm("Deleting zone : " + domain)){
		return;
	}

	var form = document.goForm;
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goTest(id){
	var form = document.goForm;
	form.actionFlag.value = "test";
	form.id.value = id;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">Zone Transfer</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Domain</td>
		<td>
			<input type="text" name="domain" size="25"> ex) nxfilter.local
		</td>
	</tr>

	<tr>
		<td>DNS Server IP</td>
		<td>
			<input type="text" name="ip" size="25">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="3"></td></tr>
	<tr class="head">
		<td width="300">Domain</td>
		<td width="">DNS Server IP</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="3"></td></tr>

<%
List<ZoneTransferData> dataList = dao.selectList();

if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='3' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	ZoneTransferData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='3'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.domain%></td>
		<td><%= data.ip%></td>
		<td align="right">
		<input type="button" value="TEST" onclick="javascript:goTest(<%= data.id%>)">
		<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.domain%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="3"></td></tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
