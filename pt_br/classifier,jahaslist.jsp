<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(JahaslistDao dao){
	JahaslistData data = new JahaslistData();
	data.domain = paramString("domain");
	data.categoryId = paramInt("categoryId");

	// Param validation.
	if(isEmpty(data.domain)){
		return;
	}

    if(data.categoryId == 0){
		errList.add("Invalid category.");
		return;
	}

	String[] arr = data.domain.split("\\s+");
	for (String domain : arr) {
		domain = domain.trim();

		if (!isValidDomain(domain)) {
			errList.add("Invalid domain. - " + domain);
			return;
		}
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(JahaslistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void deleteAll(JahaslistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.deleteAll()){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void restoreDefault(JahaslistDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.restoreDefault()){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
JahaslistDao dao = new JahaslistDao();
dao.page = paramInt("page", 1);
dao.addKw(paramString("kw"));

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("deleteAll")){
	deleteAll(dao);
}
if(actionFlag.equals("restoreDefault")){
	restoreDefault(dao);
}
if(actionFlag.equals("export")){
	String filename = dao.exportFile();

	if(isNotEmpty(filename)){
		response.sendRedirect("download.jsp?filename=" + filename + "&contentType=text/plain");
		return;
	}
	else{
		errList.add("Couldn't write the file.");
	}
}

// If it's about importation.
int importCount = paramInt("importCount");
if(importCount > 0){
	if(actionFlag.equals("ruleset")){
		succList.add(importCount + " classification rules imported.");
	}
	else{
		succList.add(importCount + " domains imported.");
	}
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gKw = paramString("kw");

if(isEmpty(gKw) && dao.selectCount() < 2000000){
	warnList.add("Jahaslist size is too small. You may have an incomplete update.");
}
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionExport(){
	var form = document.goForm;
	form.actionFlag.value = "export";
	form.submit();
}

//-----------------------------------------------
function actionImport(uploadForm){
	if(uploadForm.file1.value == ""){
		alert("No file selected.");
		return;
	}

	uploadForm.action = "import.jsp";
	uploadForm.actionFlag.value = "jahaslist";
	uploadForm.enctype = "multipart/form-data";
	uploadForm.submit();
}

//-----------------------------------------------
function actionDelete(id, domain){
	if(!confirm("Deleting domain : " + domain)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionDeleteAll(){
	if(!confirm("Delete all?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "deleteAll";
	form.submit();
}

//-----------------------------------------------
function actionRestoreDefault(){
	if(!confirm("Restore default?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "restoreDefault";
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function goSearch(kw){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.kw.value = document.all("searchKw").value;
	form.page.value = "1";
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">
<input type="hidden" name="originPage" value="<%= getPageName()%>">

<div class="title">Jahaslist</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="200">Domain</td>
		<td>
			You can add mutiple domains separated by spaces.<br>
			<textarea name="domain" cols="80" rows="2"></textarea>
		</td>
	</tr>

	<tr>
		<td>Category</td>
		<td>
<select name="categoryId">
<option value="0">Select a category
<%
Map<Integer,String> jahasCategoryMap = dao.getJahasCategoryMap();
for(Map.Entry<Integer,String> entry : jahasCategoryMap.entrySet()){
	int categoryId = entry.getKey();
	String categoryName = entry.getValue();
	printf("<option value='%s'>%s", categoryId, categoryName);
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="DELETE ALL" onclick="javascript:actionDeleteAll();">
<input type="button" value="RESTORE DEFAULT" onclick="javascript:actionRestoreDefault();">
<input type="button" value="EXPORT" onclick="javascript:actionExport();">
<input type="button" value="IMPORT" onclick="javascript:actionImport(this.form);">
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
<div class="div-upload">
<button>Select a file...</button>
<input type="file" name="file1">
</div>
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td width="50%">
		</td>
		<td align="right">
			<input type="text" name="searchKw" size="25" value="<%= gKw%>"
				onkeypress="javascript:if(event.keyCode == 13){goSearch(); return;}">
			<input type="button" value="SEARCH" onclick="javascript:goSearch()">
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="3"></td></tr>
	<tr class="head">
		<td width="400">Domain</td>
		<td width="">Category</td>
		<td width="100"></td>
	</tr>
	<tr class="line"><td colspan="3"></td></tr>

<%
List<JahaslistData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='3' align='center'>" + NumberFormat.getIntegerInstance().format(gCount)
		+ " domains classified at the moment.</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	JahaslistData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='3'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.domain%></td>
		<td><%= data.categoryName%></td>
		<td align="right">
			<input type="button" class="listbutton" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.domain%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="3"></td></tr>
</table>

<%if(isNotEmpty(gKw)){%>
<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
<%}%>

</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="kw" value="<%= gKw%>">
<input type="hidden" name="id" value="">
<input type="hidden" name="domain" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
