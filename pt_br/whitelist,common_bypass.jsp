<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(CommonBypassDao dao){
	CommonBypassData data = new CommonBypassData();
	data.bypassAuth = paramBoolean("bypassAuth");
	data.bypassFilter = paramBoolean("bypassFilter");
	data.domain = paramString("domain");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
CommonBypassDao dao = new CommonBypassDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
CommonBypassData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<div class="title">Common Bypass</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height: 20px">
This is for globally bypassed domains. You can add your whitelisted domains en masse here.
</div>
		</td>
	</tr>

	<tr>
		<td width="200">Bypass Auth</td>
		<td>
		<input type="checkbox" class="no-border" name="bypassAuth"
			<%if(data.bypassAuth){out.print("checked");}%>>
	</tr>

	<tr>
		<td>Bypass Filter</td>
		<td>
		<input type="checkbox" class="no-border" name="bypassFilter"
			<%if(data.bypassFilter){out.print("checked");}%>>
	</tr>

	<tr>
		<td>Domain</td>
		<td>
			<textarea name="domain" cols="80" rows="32"><%= data.domain%></textarea>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
