<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void updateProxy(PolicyProxyDao dao){
	PolicyProxyData data = new PolicyProxyData();
	data.enableProxy = paramBoolean("enableProxy");
	data.blockHttps = paramBoolean("blockHttps");
	data.blockIpHost = paramBoolean("blockIpHost");
	data.blockOtherBrowser = paramBoolean("blockOtherBrowser");
	data.blockedKeyword = paramString("blockedKeyword");
	data.ieProxyBypass = paramString("ieProxyBypass");
	data.cacheTtl = paramInt("cacheTtl");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void updateAppCon(PolicyApplicationDao dao){
	PolicyApplicationData data = new PolicyApplicationData();
	data.enableControl = paramBoolean("enableControl");
	data.executionInterval = paramInt("executionInterval");
	data.blockUltrasurf = paramBoolean("blockUltrasurf");
	data.blockTor = paramBoolean("blockTor");
	data.pname = paramString("pname");
	data.exclude = paramString("exclude");

	// Param validation.
	String kw = dao.findInvalidKw(data.pname);
	if (isNotEmpty(kw)) {
		errList.add("Invalid keyword for blocked process name, minimum length = 4! - " + kw);
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
PolicyProxyDao proxyDao = new PolicyProxyDao();
PolicyApplicationDao appConDao = new PolicyApplicationDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("updateProxy")){
	updateProxy(proxyDao);
}
if(actionFlag.equals("updateAppCon")){
	updateAppCon(appConDao);
}

// Global.
PolicyProxyData proxyData = proxyDao.selectOne();
PolicyApplicationData appConData = appConDao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="updateProxy">

<div class="title">Proxy Filter</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Enable Proxy</td>
		<td>
		<input type="checkbox" class="no-border" name="enableProxy"
			<%if(proxyData.enableProxy){out.print("checked");}%>> Filtering for IE, Chrome, Firefox.
			</td>
	</tr>

	<tr>
		<td>Block IP host</td>
		<td><input type="checkbox" class="no-border" name="blockIpHost"
			<%if(proxyData.blockIpHost){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Block Other Browser</td>
		<td>
		<input type="checkbox" class="no-border" name="blockOtherBrowser"
			<%if(proxyData.blockOtherBrowser){out.print("checked");}%>>
			Block any program making direct HTTP connection to the Internet.
			</td>
	</tr>

	<tr>
		<td>IE Proxy Bypass</td>
		<td>
			This is for bypassing NxClient using IE proxy setup. You can add multiple domains separated by semicolons.<br>
			&nbsp;&nbsp;ex) https://*.google.com;http://www.nxfilter.org:8080<br>
			<textarea name="ieProxyBypass" cols="80" rows="2"><%= proxyData.ieProxyBypass%></textarea>
		</td>
	</tr>

	<tr>
		<td>Query Cache TTL</td>
		<td><input type="text" name="cacheTtl" value="<%= proxyData.cacheTtl%>" size="2"> seconds, 60 ~ 3600</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="updateAppCon">

<div class="title">Application Control</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	</tr>

	<tr>
		<td width="200">Enable Control</td>
		<td>
		<input type="checkbox" class="no-border" name="enableControl"
			<%if(appConData.enableControl){out.print("checked");}%>>
			</td>
	</tr>

	<tr>
		<td>Block UltraSurf</td>
		<td><input type="checkbox" class="no-border" name="blockUltrasurf"
			<%if(appConData.blockUltrasurf){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Block Tor</td>
		<td><input type="checkbox" class="no-border" name="blockTor"
			<%if(appConData.blockTor){out.print("checked");}%>></td>
	</tr>

	<tr>
		<td>Blocked Process Name</td>
		<td>
			Blocking programs by keyword matching against the process names. You can add keywords separated<br>
			by spaces. For exact matching, use square brackets.<br>
			&nbsp;&nbsp;ex) Skype [uTorrent.exe] Dropbox<br>
			<textarea name="pname" cols="80" rows="4"><%= appConData.pname%></textarea>
		</td>
	</tr>

	<tr>
		<td>Excluded Keywords</td>
		<td>
			Add process names or keywords to bypass process blocking.<br>
			&nbsp;&nbsp;ex) Explorer Firefox<br>
			<textarea name="exclude" cols="80" rows="2"><%= appConData.exclude%></textarea>
		</td>
	</tr>

	<tr>
		<td>Execution Interval</td>
		<td><input type="text" name="executionInterval" size="4" maxlength="4"
			value="<%= appConData.executionInterval%>"> seconds, 15 ~ 300</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
