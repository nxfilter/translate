<%@include file="include/top.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object for chart.
H2ReportDao reportDao = new H2ReportDao();
ReportStatsData stats = reportDao.getStats();
ReportChartData requestTrend = reportDao.getRequestTrend();
ReportChartData domainTop = reportDao.getDomainTop(5);
ReportChartData categoryTop = reportDao.getCategoryTop(5);

// Create data access object for blocked list.
RequestDao requestDao = new RequestDao();
requestDao.page = 1;
requestDao.limit = 10;
requestDao.stime = strftimeAdd("yyyyMMddHHmm", 60 * 60 * -12);  // 12 hours ago.
requestDao.etime = strftime("yyyyMMddHHmm");
requestDao.blockFlag = true;

if(isNewLogin() && isFreeJahaslist()){
	warnList.add("We're using free Jahaslist license for 20 users.");
}

// Message check.
checkNewMessage();

// Version check.
checkNewVersion();
%>
<%@include file="include/action_info.jsp"%>

<!-- Google chart -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});

// Draw request trend chart.
google.setOnLoadCallback(drawRequestTrend);
function drawRequestTrend() {
	var data = google.visualization.arrayToDataTable([
		["Time", "Request"]
<%
List<String[]> arrList = requestTrend.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Request trend",
		legend: {position: "none"}
	};

	var chart = new google.visualization.LineChart(document.getElementById("chartRequestTrend"));
	chart.draw(data, options);
}

// Draw block trend chart.
google.setOnLoadCallback(drawBlockTrend);
function drawBlockTrend() {
	var data = google.visualization.arrayToDataTable([
		["Time", "Block"]
<%
arrList = requestTrend.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Block trend",
		legend: {position: "none"}
	};

	var chart = new google.visualization.LineChart(document.getElementById("chartBlockTrend"));
	chart.draw(data, options);
}

// Draw domain top chart.
google.setOnLoadCallback(drawDomainTop);
function drawDomainTop() {
	var data = google.visualization.arrayToDataTable([
		["Domain", "Request"]
<%
arrList = domainTop.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 - Domínios/Req",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartDomainTop"));
	chart.draw(data, options);
}

// Draw domain block chart.
google.setOnLoadCallback(drawDomainBlock);
function drawDomainBlock() {
	var data = google.visualization.arrayToDataTable([
		["Domain", "Block"]
<%
arrList = domainTop.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 - Domínios/Bloqueio",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartDomainBlock"));
	chart.draw(data, options);
}

// Draw category top chart.
google.setOnLoadCallback(drawCategoryTop);
function drawCategoryTop() {
	var data = google.visualization.arrayToDataTable([
		["Category", "Request"]
<%
arrList = categoryTop.getDataList();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 - Categorias/Req",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartCategoryTop"));
	chart.draw(data, options);
}

// Draw category block chart.
google.setOnLoadCallback(drawCategoryBlock);
function drawCategoryBlock() {
	var data = google.visualization.arrayToDataTable([
		["Category", "Block"]
<%
arrList = categoryTop.getDataListBlocked();
for(int i = 0; i < arrList.size(); i++){
	String[] arr = arrList.get(i);

	printf(", ['%s', %s]", arr[0], arr[1]);
}
%>
	]);

	var options = {
		title: "Top 5 - Categorias/Bloqueio",
        pieHole: 0.4
	};

	var chart = new google.visualization.PieChart(document.getElementById("chartCategoryBlock"));
	chart.draw(data, options);
}
</script>
<!-- /Google chart -->

<div class="title">
<%= reportDao.getStime()%> ~ <%= reportDao.getEtime()%>
</div>
<div class="stats">
request-sum = <%= stats.reqSum%>,
request-cnt = <%= stats.reqCnt%>,
block-sum = <%= stats.blockSum%>,
block-cnt = <%= stats.blockCnt%>,
domain = <%= stats.domainCnt%>,
user = <%= stats.userCnt%>,
client-ip = <%= stats.cltIpCnt%>
</div>

<table width="98%" cellpadding="0" cellspacing="0">
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td width="650">
    <div id="chartRequestTrend" style="width: 650px; height: 300px;"></div>
	</td>
	<td width="650">
    <div id="chartBlockTrend" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td>
    <div id="chartDomainTop" style="width: 650px; height: 300px;"></div>
	</td>
	<td>
    <div id="chartDomainBlock" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
<tr class="line"><td colspan="3"></td></tr>

<tr>
	<td>
    <div id="chartCategoryTop" style="width: 650px; height: 300px;"></div>
	</td>
	<td>
    <div id="chartCategoryBlock" style="width: 650px; height: 300px;"></div>
	</td>

	<td>
	</td>
</tr>
</table>

<!-- Recently Blocked Request -->
<p>

<div class="list">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="11"></td></tr>
	<tr class="head">
		<td width="90">Time</td>
		<td width="50">Block</td>
		<td width="50" align="center">Count</td>
		<td width="60" align="center">Type</td>
		<td width="250">Domain</td>
		<td width="120">User</td>
		<td width="120">Client IP</td>
		<td width="120">Group</td>
		<td width="120">Policy</td>
		<td width="180">Category</td>
		<td width="">Reason</td>
	</tr>
	<tr class="line"><td colspan="11"></td></tr>

<%
List<RequestData> dataList = requestDao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='11' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	RequestData data = dataList.get(i);

	String categoryLine = data.category;
	if(categoryLine.length() > 30){
		categoryLine = safeSubstring(data.category, 30) + "..";
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='11'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td align="center"><%= data.getBlockYn()%></td>
		<td align="right"><%= data.cnt%>&nbsp;&nbsp;</td>
		<td align="center"><%= data.getTypeCode()%></td>
		<td><%= data.domain%></td>
		<td><%= data.user%></td>
		<td><%= data.cltIp%></td>
		<td title="<%= data.grp%>"><%= data.getFirstGrp()%></td>
		<td><%= data.policy%></td>
		<td title="<%= data.category%>"><%= categoryLine%></td>
		<td><%= data.getReason()%></td>
	</tr>
<%}%>

	<tr class="line"><td colspan="11"></td></tr>
</table>
</div>

<%@include file="include/bottom.jsp"%>
