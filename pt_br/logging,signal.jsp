<%@include file="include/top.jsp"%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();
permission.addReport();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
SignalDao dao = new SignalDao();

// Set filtering option.
dao.limit = 100;
dao.page = paramInt("page", 1);
dao.stime = paramString("stime");
dao.etime = paramString("etime");

dao.user = paramString("user");
dao.cltIp = paramString("cltIp");
dao.signalPing = paramBoolean("signalPing");
dao.signalStart = paramBoolean("signalStart");

dao.signalStop = paramBoolean("signalStop");
dao.signalSwitch = paramBoolean("signalSwitch");
dao.signalIpupdate = paramBoolean("signalIpupdate");

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gTimeOption = paramString("timeOption", "2h");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function setUserdef(form){
	form.timeOption[0].checked = true;
}

//-----------------------------------------------
function setPeriod(form){
	var opt = radioGetValue(form.timeOption);

	var stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 2)%>';
	var etime = '<%= strftime("yyyy/MM/dd HH:mm")%>';

	if(opt == '24h'){
		stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 24)%>';
	}

	if(opt == '48h'){
		stime = '<%= strftimeAdd("yyyy/MM/dd HH:mm", -3600 * 48)%>';
	}

	form.stime.value = stime;
	form.etime.value = etime;
	form.stime.disabled = false;
	form.etime.disabled = false;
}

//-----------------------------------------------
function setPeriod2(form){
	if(!radioIsChecked(form.timeOption) || form.timeOption[0].checked){
		return;
	}
	setPeriod(form);
}

//-----------------------------------------------
function clearSearchForm(){
	document.searchForm.user.value = "";
	document.searchForm.cltIp.value = "";
}

//-----------------------------------------------
function searchByUser(user){
	clearSearchForm();
	document.searchForm.user.value = user;
	document.searchForm.submit();
}

//-----------------------------------------------
function searchByCltIp(cltIp){
	clearSearchForm();
	document.searchForm.cltIp.value = cltIp;
	document.searchForm.submit();
}
</script>

<!-- view -->
<form name="searchForm" action="<%= getPageName()%>" method="get">
<div class="title">Signal</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="100" align="right">Time :</td>
		<td colspan="7">
			<input id="stime" type="text" name="stime" value="<%= dao.getStime()%>" size="12" onchange="javascript:setUserdef(this.form)">
				~ <input id="etime" type="text" name="etime" value="<%= dao.getEtime()%>" size="12" onchange="javascript:setUserdef(this.form)">

			<div style="display: none;">
				<input type="radio" class="no-border" name="timeOption" value="userdef" onclick="javascript:setPeriod(this.form)"
					<%if(gTimeOption.equals("userdef")){out.print("checked");}%>> User defined
			</div>

			<input type="radio" class="no-border" name="timeOption" value="2h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("2h")){out.print("checked");}%>> Last 2 hours
			<input type="radio" class="no-border" name="timeOption" value="24h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("24h")){out.print("checked");}%>> Last 24 hours
			<input type="radio" class="no-border" name="timeOption" value="48h" onclick="javascript:setPeriod(this.form)"
				<%if(gTimeOption.equals("48h")){out.print("checked");}%>> Last 48 hours
		</td>
	</tr>

	<tr>
		<td width="100" align="right">User :</td>
		<td width="100"><input type="text" name="user" value="<%= dao.user%>" size="25"></td>

		<td width="100" align="right">Client IP :</td>
		<td width="100"><input type="text" name="cltIp" value="<%= dao.cltIp%>" size="25"></td>
		
		<td width="100"></td>
		<td width="100"></td>

		<td width="60"></td>
		<td ></td>
	</tr>

	<tr>
		<td width="100" align="right">Signal :</td>
		<td width="100" colspan="6">
			<input type="checkbox" class="no-border" name="signalPing"	<%if(dao.signalPing){out.print("checked");}%>>PING
			<input type="checkbox" class="no-border" name="signalStart" <%if(dao.signalStart){out.print("checked");}%>>START
			<input type="checkbox" class="no-border" name="signalStop" <%if(dao.signalStop){out.print("checked");}%>>STOP
			<input type="checkbox" class="no-border" name="signalIpupdate" <%if(dao.signalIpupdate){out.print("checked");}%>>IPUPDATE
		</td>

		<td width="60"></td>
		<td ></td>
	</tr>

	<tr height="30">
		<td></td>
		<td></td>
		<td></td>
		<td colspan="5">
<input type="button" value="SUBMIT" onclick="javascript:setPeriod2(this.form);this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="CLEAR" onclick="javascript:clearSearchForm();">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td>
			Count : <%= gCount%> / Page : <%= dao.page%>
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="4"></td></tr>
	<tr class="head">
		<td width="100">Time</td>
		<td width="200">User</td>
		<td width="150">Client IP</td>
		<td width="">Signal</td>
	</tr>
	<tr class="line"><td colspan="4"></td></tr>

<%
List<SignalData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='4' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	SignalData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='4'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td><a href="javascript:searchByUser('<%= data.user%>')"><%= data.user%></a></td>
		<td><a href="javascript:searchByCltIp('<%= data.cltIp%>')"><%= data.cltIp%></a></td>
		<td><%= data.signal%></td>
	</tr>
<%}%>

	<tr class="line"><td colspan="4"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="limit" value="<%= gLimit%>">
<input type="hidden" name="timeOption" value="<%= gTimeOption%>">
<input type="hidden" name="stime" value="<%= dao.stime%>">
<input type="hidden" name="etime" value="<%= dao.etime%>">
<input type="hidden" name="user" value="<%= dao.user%>">
<input type="hidden" name="cltIp" value="<%= dao.cltIp%>">
<input type="hidden" name="signalPing" value="<%= dao.signalPing%>">
<input type="hidden" name="signalStart" value="<%= dao.signalStart%>">
<input type="hidden" name="signalStop" value="<%= dao.signalStop%>">
<input type="hidden" name="signalIpupdate" value="<%= dao.signalIpupdate%>">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>

<!-- Datetime picker -->
<script type="text/javascript">
//-----------------------------------------------
var dateToDisable = new Date();

//-----------------------------------------------
jQuery("#stime").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});

//-----------------------------------------------
jQuery("#etime").datetimepicker({
	format: "Y/m/d H:i",
	step: 1,
	beforeShowDay: function(date) {
		if (date.getMonth() > dateToDisable.getMonth()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
</script>
