<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void delete(ExcludedDao dao){
	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void deleteAll(ExcludedDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.deleteAll()){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ExcludedDao dao = new ExcludedDao();
dao.page = paramInt("page", 1);
dao.addKw(paramString("kw"));

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("deleteAll")){
	deleteAll(dao);
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gKw = paramString("kw");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, domain){
	if(!confirm("Deleting data : " + domain)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionDeleteAll(){
	if(!confirm("Delete all?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "deleteAll";
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function goSearch(kw){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.kw.value = document.all("searchKw").value;
	form.page.value = "1";
	form.submit();
}

//-----------------------------------------------
function setPopupForm(domain){
	document.popupForm.domain.value = domain;
	document.popupForm.id.value = 0;
	document.popupForm.chkSubdomain.checked = false;
	$("#popupResult").empty();
}
</script>

<!-- view -->
<div class="title">Excluded</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td>
List of excluded domains from classification for having critical errors. NxClassifier doesn't do classification
against any domain in this list.
		</td>
	</tr>

	<tr>
		<td>
		&nbsp;&nbsp;<input type="button" value="DELETE ALL"
			onclick="javascript:actionDeleteAll()">
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td width="50%">
			Count : <%= gCount%> / Page : <%= gPage%>
		</td>
		<td align="right">
			<input type="text" name="searchKw" size="25" value="<%= gKw%>"
				onkeypress="javascript:if(event.keyCode == 13){goSearch(); return;}">
			<input type="button" value="SEARCH" onclick="javascript:goSearch()">
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="4"></td></tr>
	<tr class="head">
		<td width="100">Time</td>
		<td width="250">Domain</td>
		<td width="">Reason</td>
		<td width="150"></td>
	</tr>
	<tr class="line"><td colspan="4"></td></tr>

<%
List<ExcludedData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='4' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	ExcludedData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='4'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.getCtime()%></td>
		<td><%= data.domain%></td>
		<td><%= data.reason%></td>
		<td align="right">
			<input type="button" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.domain%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="4"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="kw" value="<%= gKw%>">
<input type="hidden" name="id" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
