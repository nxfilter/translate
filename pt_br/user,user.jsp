<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(UserDao dao){
	UserData data = new UserData();
	data.name = paramString("name");
	data.description = paramString("description");

	// Param validation.
	if(!ParamTest.isValidNameLen(data.name)){
		errList.add(ParamTest.ERR_NAME_LEN);
		return;
	}
	
	if(!ParamTest.isValidUsernameChar(data.name)){
		errList.add(ParamTest.ERR_USERNAME_CHAR);
		return;
	}

	if (ParamTest.isDupUser(data.name)) {
		errList.add("User already exists.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(UserDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
UserDao dao = new UserDao();
dao.limit = 30;
dao.page = paramInt("page", 1);
dao.addKw(paramString("kw"));

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gKw = paramString("kw");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionDelete(id, name){
	if(!confirm("Deleting user : " + name)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function goSearch(kw){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.kw.value = document.all("searchKw").value;
	form.page.value = "1";
	form.submit();
}

//-----------------------------------------------
function goEdit(id){
	var form = document.goForm;
	form.action = "user,user_edit.jsp";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function goTest(name){
	var form = document.goForm;
	form.action = "user,user_test.jsp";
	form.name.value = name;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">

<div class="title">User</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

	<tr>
		<td width="200">Name</td>
		<td>
			<input type="text" name="name" size="25">
		</td>
	</tr>

	<tr>
		<td>Description</td>
		<td>
			<input type="text" name="description" size="50">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td width="50%">
			Count : <%= gCount%> / Page : <%= gPage%>
		</td>
		<td align="right">
			<input type="text" name="searchKw" size="25" value="<%= gKw%>"
				onkeypress="javascript:if(event.keyCode == 13){goSearch(); return;}">
			<input type="button" value="SEARCH" onclick="javascript:goSearch()">
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="7"></td></tr>
	<tr class="head">
		<td width="200">Name</td>
		<td width="50">Type</td>
		<td width="250">Policy</td>
		<td width="300">IP</td>
		<td width="300">Group</td>
		<td width="">Description</td>
		<td width="150"></td>
	</tr>
	<tr class="line"><td colspan="7"></td></tr>

<%
List<UserData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='7' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	UserData data = dataList.get(i);

	String ipLine = data.getIpLine();
	if(ipLine.length() > 40){
		ipLine = ipLine.substring(0, 40) + "..";
	}

	String groupLine = data.getGroupLine();
	if(groupLine.length() > 40){
		groupLine = groupLine.substring(0, 40) + "..";
	}

	String nameStyle = "";
	if(data.getType().equals("E")){
		nameStyle = "text-decoration:line-through;";
	}

	// Add free-time policy.
	if(!isEmpty(data.ftPolicyName)){
		data.policyName += " / " + data.ftPolicyName;
	}

	if(i > 0){
		out.println("<tr class='line2'><td colspan='7'></td></tr>");
	}
%>
	<tr class="row">
		<td style="<%= nameStyle%>"><%= data.name%></td>
		<td><%= data.getType()%></td>
		<td><%= data.policyName%></td>
		<td><%= ipLine%></td>
		<td><%= groupLine%></td>
		<td><%= data.description%></td>
		<td align="right">
			<input type="button" class="listbutton" value="TEST" onclick="javascript:goTest('<%= data.name%>')">
			<input type="button" class="listbutton" value="EDIT" onclick="javascript:goEdit(<%= data.id%>)">
			<input type="button" class="listbutton" value="DEL" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.name%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="7"></td></tr>
</table>
<p>
* I = IP, P = Password, L = LDAP, E = Expired

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="kw" value="<%= gKw%>">
<input type="hidden" name="id" value="">
<input type="hidden" name="name" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
