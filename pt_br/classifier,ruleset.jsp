<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void insert(ClassifierRulesetDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	ClassifierRulesetData data = new ClassifierRulesetData();
	
	data.target = paramString("target");
	data.keyword = paramString("keyword");
	data.points = paramInt("points");
	data.categoryId = paramInt("categoryId");

	// Param validation.
    if(isEmpty(data.keyword)){
		errList.add("Invalid keyword.");
		return;
	}

    if(data.categoryId == 0){
		errList.add("Invalid category.");
		return;
	}

	if(!ParamTest.isValidRegex(data.keyword)){
		errList.add(ParamTest.ERR_REGEX_VALID);
		return;
	}

    if(ParamTest.isDupClassificationRule(data.target, data.keyword, data.categoryId)){
		errList.add("Rule already exists.");
		return;
	}

	if(dao.insert(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void delete(ClassifierRulesetDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.delete(paramInt("id"))){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void deleteAll(ClassifierRulesetDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.deleteAll()){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void restoreDefault(ClassifierRulesetDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.restoreDefault()){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void resetHits(ClassifierRulesetDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	if(dao.resetHits()){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void update(ClassifierRulesetDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	ClassifierRulesetData data = new ClassifierRulesetData();
	
	data.id = paramInt("id");
	data.keyword = paramString("keyword");
	data.points = paramInt("points");

	// Param validation.
    if(isEmpty(data.keyword)){
		errList.add("Invalid keyword.");
		return;
	}

	if(!ParamTest.isValidRegex(data.keyword)){
		errList.add(ParamTest.ERR_REGEX_VALID);
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
ClassifierRulesetDao dao = new ClassifierRulesetDao();
dao.limit = 100;
dao.page = paramInt("page", 1);
dao.addKw(paramString("kw"));

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("insert")){
	insert(dao);
}
if(actionFlag.equals("delete")){
	delete(dao);
}
if(actionFlag.equals("deleteAll")){
	deleteAll(dao);
}
if(actionFlag.equals("restoreDefault")){
	restoreDefault(dao);
}
if(actionFlag.equals("resetHits")){
	resetHits(dao);
}
if(actionFlag.equals("export")){
	String filename = "ruleset_" + strftime("yyyyMMddHHmm") + ".txt";

	if(dao.exportFile(filename)){
		response.sendRedirect("download.jsp?filename=" + filename + "&contentType=text/plain");
		return;
	}
	else{
		errList.add("Couldn't write the file.");
	}
}
if(actionFlag.equals("update")){
	update(dao);
}

// If it's about importation.
int importCount = paramInt("importCount");
if(importCount > 0){
	if(actionFlag.equals("ruleset")){
		succList.add(importCount + " classification rules imported.");
	}
	else{
		succList.add(importCount + " domains imported.");
	}
}

// Global.
int gCount = dao.selectCount();
int gPage = dao.page;
int gLimit = dao.limit;
String gKw = paramString("kw");
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionExport(){
	var form = document.goForm;
	form.actionFlag.value = "export";
	form.submit();
}

//-----------------------------------------------
function actionImport(uploadForm){
	if(uploadForm.file1.value == ""){
		alert("No file selected.");
		return;
	}

	uploadForm.action = "import.jsp";
	uploadForm.actionFlag.value = "ruleset";
	uploadForm.enctype = "multipart/form-data";
	uploadForm.submit();
}

//-----------------------------------------------
function actionDelete(id, keyword){
	if(!confirm("Deleting keyword : " + keyword)){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "delete";
	form.id.value = id;
	form.submit();
}

//-----------------------------------------------
function actionDeleteAll(){
	if(!confirm("Delete all?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "deleteAll";
	form.submit();
}

//-----------------------------------------------
function actionRestoreDefault(){
	if(!confirm("Restore default?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "restoreDefault";
	form.submit();
}

//-----------------------------------------------
function actionResetHits(){
	if(!confirm("Reset Hits?")){
		return;
	}

	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "resetHits";
	form.submit();
}

//-----------------------------------------------
function actionUpdate(id){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.actionFlag.value = "update";
	form.id.value = id;
	form.keyword.value = $("#keyword_" + id).val();
	form.points.value = $("#points_" + id).val();
	form.submit();
}

//-----------------------------------------------
function goPage(page){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.page.value = page;
	form.submit();
}

//-----------------------------------------------
function goSearch(kw){
	var form = document.goForm;
	form.action = "<%= getPageName()%>";
	form.kw.value = document.all("searchKw").value;
	form.page.value = "1";
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="insert">
<input type="hidden" name="originPage" value="<%= getPageName()%>">

<div class="title">Ruleset</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Keyword</td>
		<td>
			<input type="text" name="keyword" size="100">
		</td>
	</tr>

	<tr>
		<td>Target</td>
		<td>
<select name="target">
<%
Map<String,String> targetMap = dao.getTargetMap();
for(Map.Entry<String,String> entry : targetMap.entrySet()){
	String kw = entry.getKey();
	String val = entry.getValue();
	printf("<option value='%s'>%s", kw, val);
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td>Points</td>
		<td>
			<input type="text" name="points" size="3" value="0"> -1000 ~ 1000,
			<input type="button" value="SET-MAX-POINTS" onclick="javascript:this.form.points.value=1000;">
		</td>
	</tr>

	<tr>
		<td>Category</td>
		<td>
<select name="categoryId">
<option value="0">Select a category
<%
Map<Integer,String> jahasCategoryMap = dao.getJahasCategoryMap();
for(Map.Entry<Integer,String> entry : jahasCategoryMap.entrySet()){
	int categoryId = entry.getKey();
	String categoryName = entry.getValue();
	printf("<option value='%s'>%s", categoryId, categoryName);
}
%>
</select>
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
<input type="button" value="RESET HITS" onclick="javascript:actionResetHits();">
<input type="button" value="DELETE ALL" onclick="javascript:actionDeleteAll();">
<input type="button" value="RESTORE DEFAULT" onclick="javascript:actionRestoreDefault();">
<input type="button" value="EXPORT" onclick="javascript:actionExport();">
<input type="button" value="IMPORT" onclick="javascript:actionImport(this.form);">
		</td>
	</tr>

	<tr>
		<td></td>
		<td>
<div class="div-upload">
<button>Select a file...</button>
<input type="file" name="file1">
</div>
		</td>
	</tr>

</table>
<img src="img/pix.png" height="1" width="100%">
</form>

<!-- /view -->
<p>

<!-- list -->
<div class="list">
<table width="100%">
	<tr>
		<td width="50%">
			Count : <%= gCount%> / Page : <%= gPage%>
		</td>
		<td align="right">
			<input type="text" name="searchKw" size="25" value="<%= gKw%>"
				onkeypress="javascript:if(event.keyCode == 13){goSearch(); return;}">
			<input type="button" value="SEARCH" onclick="javascript:goSearch()">
		</td>
	</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
	<tr class="line"><td colspan="6"></td></tr>
	<tr class="head">
		<td width="150">Category</td>
		<td width="120">Target</td>
		<td width="80">Points</td>
		<td width="60">Hits</td>
		<td width="">Keyword</td>
		<td width="120"></td>
	</tr>
	<tr class="line"><td colspan="6"></td></tr>

<%
List<ClassifierRulesetData> dataList = dao.selectList();
if(dataList.isEmpty()){
	out.println("<tr class='row'>");
	out.println("<td colspan='6' align='center'>No data</td>");
	out.println("</tr>");
}

for(int i = 0; i < dataList.size(); i++){
	ClassifierRulesetData data = dataList.get(i);

	if(i > 0){
		out.println("<tr class='line2'><td colspan='6'></td></tr>");
	}
%>
	<tr class="row">
		<td><%= data.categoryName%></td>
		<td><%= data.getTarget()%></td>
		<td><input type="text" id="points_<%= data.id%>" size="2" value="<%= data.points%>" style="text-align:right;"></td>
		<td><%= data.hits%></td>
		<td><input type="text" id="keyword_<%= data.id%>" size="140" value="<%= data.keyword%>"></td>
		<td align="right">
			<input type="button" value="UPDATE" onclick="javascript:actionUpdate(<%= data.id%>)">
			<input type="button" value="DELETE" onclick="javascript:actionDelete(<%= data.id%>, '<%= data.keyword%>')">
		</td>
	</tr>
<%}%>

	<tr class="line"><td colspan="6"></td></tr>
</table>

<table border="0" width="100%" cellpadding="4">
	<tr>
		<td align="center"><%= getPagination(gCount, gLimit, gPage)%></td>
	</tr>
</table>
</div>
<!-- /list -->

<!-- goForm -->
<form action="<%= getPageName()%>" name="goForm" method="get">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="page" value="<%= gPage%>">
<input type="hidden" name="kw" value="<%= gKw%>">
<input type="hidden" name="id" value="">
<input type="hidden" name="domain" value="">
<input type="hidden" name="keyword" value="">
<input type="hidden" name="points" value="">
</form>
<!-- /goForm -->

<%@include file="include/bottom.jsp"%>
