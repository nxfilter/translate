<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(WhitelistKeywordDao dao){
	WhitelistData data = new WhitelistData();
	data.id = paramInt("id");
	data.description = paramString("description");

	data.bypassAuth = paramBoolean("bypassAuth");
	data.bypassFilter = paramBoolean("bypassFilter");
	data.bypassLog = paramBoolean("bypassLog");
	data.adminBlock = paramBoolean("adminBlock");
	data.dropPacket = paramBoolean("dropPacket");
	data.appliedPolicyArr = paramArray("appliedPolicyArr");

	if((data.bypassAuth || data.dropPacket)
		&& (data.appliedPolicyArr != null && data.appliedPolicyArr.length > 0)){

		errList.add("'Bypass Authetication' and 'Drop Packet' should be applied on global level.");
		return;
	}

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
WhitelistKeywordDao dao = new WhitelistKeywordDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
WhitelistData data = dao.selectOne(paramInt("id"));

// Get policy list.
List<PolicyData> gPolicyList = new PolicyDao().selectList();
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionUpdate(form){
	form.actionFlag.value = "update";
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="<%= data.id%>">

<!--  -->
<div class="title">Whitelist by Keyword</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Keyword</td>
		<td><%= data.keyword%></td>
	</tr>

	<tr>
		<td>Description</td>
		<td>
		<input type="text" name="description" value="<%= data.description%>" size="50">
		</td>
	</tr>

	<tr>
		<td>Bypass Authentication</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassAuth" <%if(data.bypassAuth){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Bypass Filtering</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassFilter" <%if(data.bypassFilter){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Bypass Logging</td>
		<td>
			<input type="checkbox" class="no-border" name="bypassLog" <%if(data.bypassLog){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Admin Block</td>
		<td>
			<input type="checkbox" class="no-border" name="adminBlock" <%if(data.adminBlock){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td>Drop Packet</td>
		<td>
			<input type="checkbox" class="no-border" name="dropPacket" <%if(data.dropPacket){out.print("checked");}%>>
		</td>
	</tr>

	<tr>
		<td valign="top">Applied Policy</td>
		<td>
If there's no applied policy it becomes a global whitelist.
<br>

<%
for(PolicyData pd : gPolicyList){
%>
<input type="checkbox" class="no-border"
	name="appliedPolicyArr" value="<%= pd.id%>" <%if(data.isAppliedPolicy(pd.id)){out.print("checked");}%>><%= pd.name%>
<br>
<%}%>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:actionUpdate(this.form)">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
