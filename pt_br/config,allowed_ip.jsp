<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(AllowedIpDao dao){
	if(demoFlag){
		errList.add("Not allowed on demo site.");
		return;
	}

	AllowedIpData data = new AllowedIpData();
	data.dnsAllowed = paramString("dnsAllowed");
	data.dnsBlocked = paramString("dnsBlocked");
	data.loginAllowed = paramString("loginAllowed");
	data.guiAllowed = paramString("guiAllowed");
	data.bypassAll = paramString("bypassAll");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
AllowedIpDao dao = new AllowedIpDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}

// Global.
AllowedIpData data = dao.selectOne();
%>
<%@include file="include/action_info.jsp"%>

<!-- view -->
<form action="<%= getPageName()%>" method="post">
<input type="hidden" name="actionFlag" value="update">

<div class="title">Allowed IP</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td colspan="2">
<div style="line-height:20px;">
If you add IP addresses here then any unregistered IP will be blocked.<br>
You can add IP address with the following format.
</div>
	<div class="tab">
	ex) 192.168.1 - IP addresses which start with '192.168.1'<br>
	</div>
	</p>

* You can add multiple IP addresses separated by commas.<br>
	<div class="tab">
	ex) 192.168.1,192.168.2
	</div>

		</td>
	</tr>

	<tr>
		<td colspan="2">
&nbsp;
		</td>
	</tr>

	<tr>
		<td width="200">Allowed IP For DNS</td>
		<td>
			If there's nothing added here everybody can access your DNS.<br>
			<textarea name="dnsAllowed" cols="80" rows="4"><%= data.dnsAllowed%></textarea>
		</td>
	</tr>

	<tr>
		<td colspan="2">
&nbsp;
		</td>
	</tr>

	<tr>
		<td>Blocked IP For DNS</td>
		<td>
			This is a blacklist based way of access control. It overrides 'Allowed IP for DNS'.<br>
			<textarea name="dnsBlocked" cols="80" rows="4"><%= data.dnsBlocked%></textarea>
		</td>
	</tr>

	<tr>
		<td colspan="2">
&nbsp;
		</td>
	</tr>

	<tr>
		<td>Allowed IP For Login Redirection</td>
		<td>
			If there's nothing added here all unauthenticated users will be redirected to the login-page.<br>
			<textarea name="loginAllowed" cols="80" rows="4"><%= data.loginAllowed%></textarea>
		</td>
	</tr>

	<tr>
		<td>Allowed IP To GUI</td>
		<td>
			'localhost' or '127.0.0.1' always allowed for accessing admin GUI.<br>
			<textarea name="guiAllowed" cols="80" rows="4"><%= data.guiAllowed%></textarea>
		</td>
	</tr>

	<tr>
		<td>Bypass All</td>
		<td>
			Bypass from authentication and filtering, logging.<br>
			<textarea name="bypassAll" cols="80" rows="4"><%= data.bypassAll%></textarea>
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:this.form.submit();">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
