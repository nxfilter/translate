<%@include file="include/top.jsp"%>
<%!
//-----------------------------------------------
void update(CategoryCustomDao dao){
	CategoryData data = new CategoryData();
	data.id = paramInt("id");
	data.description = paramString("description");

	if(dao.update(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void addDomain(CategoryCustomDao dao){
	CategoryDomainData data = new CategoryDomainData();
	data.categoryId = paramInt("id");
	data.domain = paramString("domain");

	// Param validation.
	if(isEmpty(data.domain)){
		return;
	}

	String[] arr = data.domain.split("\\s+");
	for (String domain : arr) {
		domain = domain.trim();

		if (!isValidDomain(domain)) {
			errList.add("Invalid domain. - " + domain);
			return;
		}
	}

	if(dao.addDomain(data)){
		succList.add("Data updated.");
	}
}

//-----------------------------------------------
void deleteDomain(CategoryCustomDao dao){
	if(dao.deleteDomain(paramInt("domainId"))){
		succList.add("Data updated.");
	}
}
%>
<%
//-----------------------------------------------
// Set permission for this page.
permission.addAdmin();

//Check permission.
if(!checkPermission()){
	return;
}

// Create data access object.
//-----------------------------------------------
CategoryCustomDao dao = new CategoryCustomDao();

// Action.
String actionFlag = paramString("actionFlag");
if(actionFlag.equals("update")){
	update(dao);
}
if(actionFlag.equals("addDomain")){
	addDomain(dao);
}
if(actionFlag.equals("deleteDomain")){
	deleteDomain(dao);
}

// Global.
CategoryData data = dao.selectOne(paramInt("id"));
%>
<%@include file="include/action_info.jsp"%>
<script type="text/javascript">
//-----------------------------------------------
function actionUpdate(form){
	form.actionFlag.value = "update";
	form.submit();
}

//-----------------------------------------------
function actionAddDomain(form){
	form.actionFlag.value = "addDomain";
	form.submit();
}

//-----------------------------------------------
function actionDeleteDomain(domainId){
	form = document.all("categoryEditForm");
	form.actionFlag.value = "deleteDomain";
	form.domainId.value = domainId;
	form.submit();
}
</script>

<!-- view -->
<form action="<%= getPageName()%>" method="post" name="categoryEditForm">
<input type="hidden" name="actionFlag" value="">
<input type="hidden" name="id" value="<%= data.id%>">
<input type="hidden" name="domainId" value="">

<!--  -->
<div class="title">Custom Category</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="200">Name</td>
		<td><%= data.name%></td>
	</tr>

	<tr>
		<td>Description</td>
		<td>
		<input type="text" name="description" value="<%= data.description%>" size="50">
		</td>
	</tr>

	<tr height="30">
		<td></td>
		<td>
<input type="button" value="SUBMIT" onclick="javascript:actionUpdate(this.form)">
<input type="button" value="RESET" onclick="javascript:this.form.reset();">
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">

<!--  -->
<div class="title">Add domain</div>

<img src="img/pix.png" height="1" width="100%">
<table width="100%" cellpadding="4" class="view">
	<tr>
		<td width="80">Domain</td>
		<td>
<div style="line-height:20px;">
You can add domains separated by spaces to a category.<br>
To include subdomains use asterisk.<br>
ex) *.nxfilter.org
</div>
		<textarea name="domain" cols="80" rows="4"></textarea>
		<div style="height: 1;"></div>
		<input type="button" value="ADD-DOMAIN" onclick="javascript:actionAddDomain(this.form)">
		</td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="2">
<%
List<CategoryDomainData> domainList = data.getDomainList();
for(int i = 0; i < domainList.size(); i++){
	CategoryDomainData cd = domainList.get(i);

	if(i > 0 && i % 5 == 0){
		out.print("<br>");
	}

	printf("<span class='domain-item'><a href='javascript:actionDeleteDomain(%s)'>[x]</a> %s</span>", cd.id, cd.domain);
}
%>
		</td>
	</tr>
</table>
<img src="img/pix.png" height="1" width="100%">

</form>
<!-- /view -->

<%@include file="include/bottom.jsp"%>
