<%@include file="lib.jsp"%>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Expires" content="-1"> 
<meta http-equiv="Pragma" content="no-cache"> 
<meta http-equiv="Cache-Control" content="no-cache">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" type="text/css" href="/lib/xdpick/jquery.datetimepicker.css"/>
<link rel="stylesheet" type="text/css" href="/lib/magnific/magnific-popup.css"/>
<link rel="stylesheet" type="text/css" href="/lib/nxlib.css?v=4342.1">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/lib/mm-styles.css?v=4338.5">
<script type="text/javascript" src="/lib/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/lib/xdpick/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="/lib/magnific/jquery.magnific-popup.js"></script>
<script type="text/javascript" src="/lib/nxlib.js?v=416"></script>
<title><%= getNxName()%> v<%= getNxVersion()%></title>
</head>

<body>

<div id="cssmenu">
	<ul>
		<li class="<%= getActiveMenu("dashboard")%>"><a href="dashboard.jsp"><i class="fa fa-fw fa-home"></i> DASHBOARD</a></li>
		<li class="<%= getActiveMenu("config")%>"><a href="config,setup.jsp"><i class="fa fa-fw fa-gear"></i> CONFIG</a>
			<ul>
				<li><a href="config,setup.jsp">Setup</a></li>
				<li><a href="config,admin.jsp">Admin</a></li>
				<li><a href="config,alert.jsp">Alerta</a></li>
				<li><a href="config,allowed_ip.jsp">IPs Permitidos</a></li>
				<li><a href="config,backup.jsp">Backup</a></li>
				<li><a href="config,block_page.jsp">Pág Bloqueio</a></li>
				<li><a href="config,vxlogon.jsp">VxLogon</a></li>
				<li><a href="config,cluster.jsp">Cluster</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("dns")%>"><a href="dns,setup.jsp"><i class="fa fa-fw fa-cloud-download"></i> DNS</a>
			<ul>
				<li><a href="dns,setup.jsp">Ajustes</a></li>
				<li><a href="dns,zone_file.jsp">Arquivo de Zona</a></li>
				<li><a href="dns,zone_transfer.jsp">Transferência de Zona</a></li>
				<li><a href="dns,redirection.jsp">Redirecionamento</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("user")%>"><a href="user,user.jsp"><i class="fa fa-fw fa-child"></i> USUÁRIO</a>
			<ul>
				<li><a href="user,user.jsp">Usários</a></li>
				<li><a href="user,group.jsp">Grupos</a></li>
				<li><a href="user,adap.jsp">Active Directory</a></li>
				<li><a href="user,ldap.jsp">OpenLDAP</a></li>
				<li><a href="user,radius.jsp">RADIUS</a></li>
				<li><a href="user,login_request.jsp">Requisição de Login</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("policy")%>"><a href="policy,policy.jsp"><i class="fa fa-fw fa-book"></i> POLÍTICA/ACL</a>
			<ul>
				<li><a href="policy,policy.jsp">Políticas</a></li>
				<li><a href="policy,free_time.jsp">Horários Livres</a></li>
				<li><a href="policy,proxy.jsp">NxClient</a></li>
				<li><a href="policy,cxblock.jsp">Chromebook</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("category")%>"><a href="category,system.jsp"><i class="fa fa-fw fa-bars"></i> CATEGORIA</a>
			<ul>
				<li><a href="category,system.jsp">Sistema</a></li>
				<li><a href="category,custom.jsp">Customização</a></li>
				<li><a href="category,domain_test.jsp">Teste de Domínios</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("classifier")%>"><a href="classifier,setup.jsp"><i class="fa fa-fw fa-bug"></i> CLASSIFICADOR</a>
			<ul>
				<li><a href="classifier,setup.jsp">Ajustes</a></li>
				<li><a href="classifier,ruleset.jsp">Regras</a></li>
				<li><a href="classifier,classified.jsp">Classificados</a></li>
				<li><a href="classifier,excluded.jsp">Excluídos</a></li>
                <li><a href="classifier,blocklist.jsp">Lista de Bloqueio</a></li>
				<li><a href="classifier,jahaslist.jsp">Jahaslist</a></li>
				<li><a href="classifier,test_run.jsp">Testes</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("whitelist")%>"><a href="whitelist,domain.jsp"><i class="fa fa-fw fa-check-circle-o"></i> WHITELIST</a>
			<ul>
				<li><a href="whitelist,domain.jsp">Domínio</a></li>
				<li><a href="whitelist,keyword.jsp">Palavras-chave</a></li>
				<li><a href="whitelist,common_bypass.jsp">Bypass Típico</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("logging")%>"><a href="logging,request.jsp"><i class="fa fa-fw fa-database"></i> LOGGING</a>
			<ul>
				<li><a href="logging,request.jsp">Requisições</a></li>
				<li><a href="logging,signal.jsp">Sinais</a></li>
				<li><a href="logging,netflow.jsp">NetFlow</a></li>
			</ul>
		</li>
		<li class="<%= getActiveMenu("report")%>"><a href="report,daily.jsp"><i class="fa fa-fw fa-bar-chart-o"></i> RELATÓRIOS</a>
			<ul>
				<li><a href="report,daily.jsp">Diário</a></li>
				<li><a href="report,weekly.jsp">Semana</a></li>
				<li><a href="report,usage.jsp">Uso</a></li>
			</ul>
		</li>
		<li><a target="_blank" href="<%= getNxTutorial()%>"><i class="fa fa-fw fa-hand-paper-o"></i> AJUDA</a>
			<ul>
				<li><a target="_blank" href="<%= getNxTutorial()%>">Tutorial</a></li>
				<li><a target="_blank" href="<%= getNxForum()%>">Google Forum</a></li>
				<li><a target="_blank" href="https://www.reddit.com/r/nxfilter">Reddit Forum</a></li>
				<li><a target="_blank" href="https://www.youtube.com/channel/UCB4UbBEYyH9b18jeRMWdohA/videos?disable_polymer=1">Canal no Youtube</a></li>
				<li><a target="_blank" href="<%= getVoteSite()%>">Vote no NxFilter</a></li>
				<li><a href="">Version <%= getNxVersion()%></a></li>
			</ul>
		</li>
		<li><a href="admin.jsp?actionFlag=logout"><i class="fa fa-fw fa-power-off"></i> LOGOUT</a>
			<ul>
				<li><a href="">Autenticado como <%= getAdminName()%></a></li>
			</ul>
		</li>
	</ul>
</div>

<div style="margin-left: 10px; margin-right: 10px;">
