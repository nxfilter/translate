<%@page import="java.util.*"%>
<%
// Error message.
for(String msg : errList){
	out.println("<div class=\"ab-alert\">");
	out.println("  <span class=\"ab-closebtn\">&times;</span>");
	out.println("<strong>Erro!</strong> " + msg);
	out.println("</div>");
}

// Success message.
for(String msg : succList){
	out.println("<div class=\"ab-alert success\">");
	out.println("  <span class=\"ab-closebtn\">&times;</span>");
	out.println("<strong>Sucesso!</strong> " + msg);
	out.println("</div>");
}

// Info message.
for(String msg : infoList){
	out.println("<div class=\"ab-alert info\">");
	out.println("  <span class=\"ab-closebtn\">&times;</span>");
	out.println("<strong>Informação!</strong> " + msg);
	out.println("</div>");
}

// Warning message.
for(String msg : warnList){
	out.println("<div class=\"ab-alert warning\">");
	out.println("  <span class=\"ab-closebtn\">&times;</span>");
	out.println("<strong>Atenção!</strong> " + msg);
	out.println("</div>");
}
%>
<script type="text/javascript">
<!--
//-----------------------------------------------
var ab_close = document.getElementsByClassName("ab-closebtn");
var ab_i;

for (ab_i = 0; ab_i < ab_close.length; ab_i++) {
    ab_close[ab_i].onclick = function(){
        var div = this.parentElement;
        div.style.opacity = "0";
        setTimeout(function(){ div.style.display = "none"; }, 600);
    }
}
//-->
</script>
